require('dotenv').config();
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const { User, encryptPW } = require('../../../models/user');

describe('generate auth token', () => {
  it('should return a valid JWT', () => {
    const payload = {
      _id: new mongoose.Types.ObjectId().toHexString(),
      username: 'john'
    };
    const user = new User(payload);
    const token = user.generateAuthToken();
    const decoded = jwt.verify(token, process.env.JWT_SECRET_KEY);

    expect(decoded).toMatchObject(payload);
  });
});

describe('password encryption', () => {
  it('should encrypt password to a length of 60', async () => {
    const BCRYPT_RESULTING_LENGTH = 60;
    const plainTextPW = 'mySecretPassword';
    const encryptedPW = await encryptPW(plainTextPW);

    expect(encryptedPW).not.toBe(plainTextPW);
    expect(encryptedPW).toHaveLength(BCRYPT_RESULTING_LENGTH);
  });
});
