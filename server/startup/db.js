const winston = require('winston');
const mongoose = require('mongoose');

const getDB = () => process.env.NODE_ENV === 'test' 
  ? process.env.MONGODB_URI_TEST
  : process.env.MONGODB_URI;

module.exports = function() {
  const db = getDB();

  mongoose.Promise = Promise;

  if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'test')
    mongoose.set('debug', true);

  mongoose
    .connect(db)
    .then(() => winston.info('Connected to the database!'));
    /* no catch method because we want our error handler 
       to log that exception and terminate the process
    */
};
