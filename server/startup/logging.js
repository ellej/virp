require('express-async-errors');    // wraps async functions in try-catch blocks
const morgan = require('morgan');
const winston = require('winston');
const { transports, format } = winston;

module.exports = function(app) {
  // log HTTP requests to console
  app.use(morgan('tiny'));

  // throw exception for each unhandled rejection and let winston catch them
  process.on('unhandledRejection', e => { throw e; });

  // log uncaught exceptions to console and file (exeptions from outside the request processing pipeline)
  winston.exceptions.handle(
    new transports.Console({
      format: format.combine(
        format.simple(),
        format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        format.prettyPrint()
      )
    }),
    new transports.File({ filename: 'uncaughtExceptions.log' })
  );

  // log errors to files (exceptions from within the request processing pipeline)
  winston.add(new transports.File({
    filename: 'errors.log',
    level: 'error'
  }));

  // log errors to console if not in production or test mode
  if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'test')
    winston.add(new transports.Console({
      format: format.combine(
        format.simple(),
        format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        format.prettyPrint()
      )
    }));
};
