const cors = require('cors');
const express = require('express');
const authRouter = require('../routes/auth');
const usersRouter = require('../routes/users');
const wakeUpRouter = require('../routes/wakeUp');
const err404Router = require('../routes/404');
const error = require('../middleware/error');
const { CLIENT_URL_DEV, CLIENT_URL_PROD, CLIENT_URL_PROD_EXTRAS } = require('../config');

module.exports = function(app) {
  app.use(cors({ origin: [CLIENT_URL_DEV, CLIENT_URL_PROD, ...CLIENT_URL_PROD_EXTRAS] }));
  app.use(express.json());
  app.use('/api/v1/auth', authRouter);
  app.use('/api/v1/users', usersRouter);
  app.use('/api/v1/wake-up', wakeUpRouter);
  app.use(err404Router);
  app.use(error);   // catches errors from the routers above
};
