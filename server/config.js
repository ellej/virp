exports.CLIENT_URL_DEV = 'http://localhost:5173';
exports.CLIENT_URL_PROD = 'https://virp.vercel.app';
exports.CLIENT_URL_PROD_EXTRAS = ['https://virp-ellej.vercel.app', 'https://virp-git-master-ellej.vercel.app'];
exports.SERVER_PORT_DEV = 3001;
