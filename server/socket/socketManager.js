const {
  // 'on' events
  USER_CONNECTED, USER_DISCONNECTED, START_CHAT, SET_MESSAGE_DESTRUCTION, 
  SEND_MESSAGE, LOAD_CHAT_MESSAGES, STORE_CRYPTO_DATA, GET_CRYPTO_DATA,
  LOAD_CONTACTS, STORE_CONTACT, GET_CONTACT_SEARCH_RES, BLOCK_CONTACT, UNBLOCK_CONTACT,
  DELETE_CONTACT, DELETE_CHAT, LOAD_UNREAD_MESSAGES, MARK_AS_READ, LOAD_SETTINGS,
  UPDATE_DEFAULT_DESTRUCTION, UPDATE_DECRYPT_BY_DEFAULT, UPDATE_NEW_MESSAGE_NOTIFICATION,
  DELETE_ACCOUNT,
  // 'emit' events
  NEW_MESSAGE, NEW_CHAT, LOADED_CHATS, LOADED_CHAT_MESSAGES, RETRIEVED_CRYPTO_DATA,
  LOADED_CONTACTS, CONTACT_SEARCH_RES, BLOCKED_BY_CONTACT, LOADED_UNREAD_MESSAGES,
  LOADED_SETTINGS, SUCCESS_DELETE_ACCOUNT, ERROR
} = require('./events');

const {
  updateUserSocketId, resetUserSocketId, loadSettings, createChat, loadChats,
  loadChatMsgs, setMsgDestruction, createMsg, getCryptoData, storeCryptoData,
  loadContacts, storeContact, loadSearchRes, blockContact, unblockContact, deleteContact,
  deleteChat, loadUnreadMsgs, markAsRead, updateDefaultMsgDestruction, updateDecryptMsgByDefault,
  updateNewMsgNotification, deleteAccount
} = require('./dbCalls');

module.exports = function(io) {
  io.on('connection', (socket) => {
    
    socket.on(USER_CONNECTED, async (userId) => {
      console.log('=== CONNECTED ===');

      updateUserSocketId({ userId, socketId: socket.id });
 
      // maybe put this in its own listener
      // the Sidebar component on client can emit LOAD_CHATS
      const chats = await loadChats(userId);
      if (chats)
        io.to(socket.id).emit(LOADED_CHATS, chats);
    });

    socket.on(GET_CRYPTO_DATA, async (userId) => {
      console.log('=== GETTING CRYPTO DATA ===');

      const cryptoData = await getCryptoData(userId);
      if (cryptoData)
        io.to(socket.id).emit(RETRIEVED_CRYPTO_DATA, cryptoData);
    });

    socket.on(LOAD_SETTINGS, async (userId) => {
      console.log('=== LOADING SETTINGS ===');

      const settings = await loadSettings(userId);
      if (settings)
        io.to(socket.id).emit(LOADED_SETTINGS, settings);
    });

    socket.on(LOAD_UNREAD_MESSAGES, async ({ userId, chats }) => {
      console.log('=== LOADING UNREAD MESSAGES ===');

      const unreadMsgs = await loadUnreadMsgs({ userId, chats });
      if (unreadMsgs && unreadMsgs.length)
        io.to(socket.id).emit(LOADED_UNREAD_MESSAGES, unreadMsgs);
    });

    socket.on(START_CHAT, async ({ senderId, receiverId }) => {
      console.log('=== STARTING CHAT ===');

      const chat = await createChat({ senderId, receiverId });
      if (!chat)
        return;

      // only emit the chat to the sender in order to not create
      // a sudden chat window pop-up for the receiver
      io.to(socket.id).emit(NEW_CHAT, chat);

      // note: make an online list in memory?
    });

    socket.on(SET_MESSAGE_DESTRUCTION, ({ chatId, userId, destructionOpt }) => {
      console.log('=== SETTING MSG DESTRUCTION ===');
      
      setMsgDestruction({ chatId, userId, destructionOpt });
    });

    socket.on(SEND_MESSAGE, async (data) => {
      console.log('=== SENDING MESSAGE ===');

      // data: { chatId, sender, receiver, msgEncryptedForSender, msgEncryptedForReceiver, pubKey }

      const res = await createMsg(data);
      if (!res)
        return;

      const { msg, chat } = res;

      const { sender, receiver/*, pubKey*/ } = data;

      // the chat object is included so that if the one listening
      // for NEW_MESSAGE does not have that chat in its active chats
      // list, it can add it directly without making another DB call
      io.to(sender.socketId).emit(NEW_MESSAGE, { msg, chat } /*{ ...msg, pubKey }*/);

      if (receiver.socketId)
        io.to(receiver.socketId).emit(NEW_MESSAGE, { msg, chat } /*{ ...msg, pubKey }*/);
    });

    socket.on(LOAD_CHAT_MESSAGES, async (chatId) => {
      console.log('=== LOADING CHAT MESSAGES ===');

      const msgs = await loadChatMsgs(chatId);
      if (!msgs)
        return;

      io.to(socket.id).emit(LOADED_CHAT_MESSAGES, msgs);
    });

    socket.on(STORE_CRYPTO_DATA, async (data) => {
      console.log('=== STORING CRYPTO DATA ===');

      // data: { userId, pubKey, encrPrivKey, salt }

      await storeCryptoData(data);
    });

    socket.on(LOAD_CONTACTS, async (userId) => {
      console.log('=== LOADING CONTACTS ===');

      const contacts = await loadContacts(userId);

      if (!contacts)
        return;

      io.to(socket.id).emit(LOADED_CONTACTS, contacts);
    });

    socket.on(STORE_CONTACT, async ({ userId, contactId }) => {
      console.log('=== STORING CONTACT ===');

      await storeContact({ userId, contactId });
    });

    socket.on(GET_CONTACT_SEARCH_RES, async ({ usernamePrefix, searcherId }) => {
      console.log('=== GETTING CONTACT SEARCH RESULT ===');

      const res = await loadSearchRes({ usernamePrefix, searcherId });

      io.to(socket.id).emit(CONTACT_SEARCH_RES, res);
    });

    socket.on(BLOCK_CONTACT, async ({ userId, contactId }) => {
      console.log('=== BLOCKING CONTACT ===');

      const res = await blockContact({ userId, contactId });

      if (res && res.contactSocketId)
        io.to(res.contactSocketId).emit(
          BLOCKED_BY_CONTACT,
          { contactId: userId, blockedByContact: true }
        );
    });

    socket.on(UNBLOCK_CONTACT, async ({ userId, contactId }) => {
      console.log('=== UNBLOCKING CONTACT ===');

      const res = await unblockContact({ userId, contactId });

      if (res && res.contactSocketId)
        io.to(res.contactSocketId).emit(
          BLOCKED_BY_CONTACT,
          { contactId: userId, blockedByContact: false }
        );
    });

    socket.on(DELETE_CONTACT, async ({ userId, contactId }) => {
      console.log('=== DELETING CONTACT ===');

      await deleteContact({ userId, contactId });
    });

    socket.on(DELETE_CHAT, async ({ userId, chatId }) => {
      console.log('=== DELETING CHAT ===');

      await deleteChat({ userId, chatId });
    });

    socket.on(MARK_AS_READ, ({ unreadMsgIds, userId }) => {
      console.log('=== MARKING MESSAGES AS READ ===');

      markAsRead({ unreadMsgIds, userId });
    });

    socket.on(UPDATE_DEFAULT_DESTRUCTION, async ({ userId, defaultMsgDestruction }) => {
      console.log('=== UPDATING DEFAULT MESSAGE DESTRUCTION SETTING ===');

      await updateDefaultMsgDestruction({ userId, defaultMsgDestruction });
    });

    socket.on(UPDATE_DECRYPT_BY_DEFAULT, async ({ userId, decryptMsgByDefault }) => {
      console.log('=== UPDATING DECRYPT MESSAGE BY DEFAULT SETTING ===');

      await updateDecryptMsgByDefault({ userId, decryptMsgByDefault });
    });

    socket.on(UPDATE_NEW_MESSAGE_NOTIFICATION, async ({ userId, newMsgNotificationOn }) => {
      console.log('=== UPDATING NEW MESSAGE NOTIFICATION SETTING ===');

      await updateNewMsgNotification({ userId, newMsgNotificationOn });
    });

    socket.on(DELETE_ACCOUNT, async ({ userId, password, token}) => {
      console.log('=== DELETING ACCOUNT ===');

      const { error, success } = await deleteAccount({ userId, password, token});

      if (success)
        io.to(socket.id).emit(SUCCESS_DELETE_ACCOUNT);
      else if (error)
        io.to(socket.id).emit(ERROR, error);
    });

    socket.on(USER_DISCONNECTED, (userId) => {
      console.log('=== USER DISCONNECTING ===');

      resetUserSocketId(userId);
      socket.disconnect();
    });

    socket.on('disconnect', () => {
      console.log('=== DISCONNECTED ===');
    });
  });
};
