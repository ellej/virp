const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { USER_COLLECTION, CHAT_COLLECTION, MESSAGE_COLLECTION } = require('../utils/collections');
const { expirations } = require('../utils/msgExpirations');

const chatSchema = new Schema({
  members: {
    type: [{
      user: {
        type: Schema.Types.ObjectId,
        ref: USER_COLLECTION,
        required: true
      },
      // a time for message destruction is also needed here for
      // each member so that each chat can have a different setting
      msgDestructionOpt: {
        type: String,
        trim: true,
        lowercase: true,
        enum: Object.values(expirations),
        required: true
        //default: expirations.DEFAULT
      }
    }],
    required: true
  },
  messages: [{
    type: Schema.Types.ObjectId,
    ref: MESSAGE_COLLECTION
  }],
  mostRecentMsgDate: {
    type: Date
  }
});

const Chat = mongoose.model(CHAT_COLLECTION, chatSchema);

function validate(chat) {
  const schema = Joi.object({
    members: Joi.array().items(Joi.object().keys({
      user: Joi.objectId().required(),
      msgDestructionOpt: Joi.string().trim().lowercase().valid(...Object.values(expirations)).required()/*default(expirations.DEFAULT)*/
    })).required(),
    messages: Joi.array().items(Joi.objectId()),
    mostRecentMsgDate: Joi.date()
});

  return schema.validate(chat);
}

exports.Chat = Chat;
exports.validateChat = validate;
