const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { USER_COLLECTION, MESSAGE_COLLECTION } = require('../utils/collections');

const messageSchema = new Schema({
  sender: {
    type: Schema.Types.ObjectId,
    ref: USER_COLLECTION,
    required: true
  },
  receiver: {
    type: Schema.Types.ObjectId,
    ref: USER_COLLECTION,
    required: true
  },
  msgEncryptedForSender: {        // the same message is stored in two encrypted versions,
    type: String,                 // one that the sender can decrypt (msgForSender),
    maxlength: 4096,              // one that the receiver can decrypt (msgForReceiver)
    required: true                // which is needed for being able to read both your sent
  },                              // and received messages at a later point in time
  msgEncryptedForReceiver: {
    type: String,
    maxlength: 4096,
    required: true
  },
  hasBeenRead: {
    type: Boolean,
    default: false
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  expireAt: {
    type: Date,
    required: true
  }
});

messageSchema.index({ expireAt: 1 }, { expireAfterSeconds: 0 });

const Message = mongoose.model(MESSAGE_COLLECTION, messageSchema);

function validate(msg) {
  const schema = Joi.object({
      sender: Joi.objectId().required(),
      receiver: Joi.objectId().required(),
      msgEncryptedForSender: Joi.string().max(4096).required(),
      msgEncryptedForReceiver: Joi.string().max(4096).required(),
      hasBeenRead: Joi.boolean(),
      createdAt: Joi.date().default(Date.now),
      expireAt: Joi.date().required()
  });

  return schema.validate(msg);
}

exports.Message = Message;
exports.validateMsg = validate;
