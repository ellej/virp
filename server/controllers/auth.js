const { User, encryptPW } = require('../models/user');

exports.signUp = async (req, res, next) => {
  let user = await User.findOne({ username: req.body.username.toLowerCase() });

  if (user)
    return next({
      status: 400,
      message: 'Username is already taken.'
    });

  const encryptedPW = await encryptPW(req.body.password);

  user = await User.create({
    username: req.body.username,
    password: encryptedPW
  });
  
  const token = user.generateAuthToken();

  res.status(201).send({
    data: {
      _id: user._id,
      username: user.username,
      token
    }
  });
};

exports.login = async (req, res, next) => {
  let user = await User.findOne({ username: req.body.username.toLowerCase() });

  if (!user)
    return next({
      status: 400,
      message: 'Invalid username or password.'
    });

  const authenticated = await user.comparePW(req.body.password);

  if (!authenticated)
    return next({
      status: 400,
      message: 'Invalid username or password.'
    });

  const token = user.generateAuthToken();

  res.send({
    data: {
      _id: user._id,
      username: user.username,
      pubKey: user.pubKey,
      encrPrivKey: user.privKey,
      salt: user.salt,
      token
    }
  });
};
