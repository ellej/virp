require('dotenv').config();         // needs to be required as early as possible
const winston = require('winston');
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const { Server } = require('socket.io');
const { CLIENT_URL_DEV, CLIENT_URL_PROD, CLIENT_URL_PROD_EXTRAS, SERVER_PORT_DEV } = require('./config');

const io = new Server(server, {
  cors: {
    origin: [CLIENT_URL_DEV, CLIENT_URL_PROD, ...CLIENT_URL_PROD_EXTRAS]
  }
});

require('./startup')(app, io);

const port = process.env.PORT || SERVER_PORT_DEV;
server.listen(port, () => winston.info(`Server running on port ${port}`));

module.exports = server;  // export used for integration testing
