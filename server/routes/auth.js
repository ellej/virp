const express = require('express');
const router = express.Router();
const { signUp, login } = require('../controllers/auth');
const { validateUser, validateLogin } = require('../models/user');
const validator = require('../middleware/validator');

// prefix: /api/v1/auth
router.post('/signup', validator(validateUser), signUp);
router.post('/login', validator(validateLogin), login);

module.exports = router;
