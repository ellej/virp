const express = require('express');
const router = express.Router();

// prefix: /api/v1/wake-up
router.get('/', (req, res) => {
  res.status(200).send();
});

module.exports = router;
