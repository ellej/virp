const express = require('express');
const router = express.Router();
const { getFromUsername, getFromId, updateCryptoData } = require('../controllers/users');
const auth = require('../middleware/auth');
const validateObjectId = require('../middleware/validateObjectId');
const validator = require('../middleware/validator');
const { validateCrypto } = require('../models/user');

// prefix: /api/v1/users
router.get('/username/:username', auth, getFromUsername);
router.get('/id/:id', [auth, validateObjectId], getFromId);
router.put('/id/:id', [auth, validateObjectId, validator(validateCrypto)], updateCryptoData);

module.exports = router;
