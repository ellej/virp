const mongoose = require('mongoose');
const isValid = mongoose.Types.ObjectId.isValid;

module.exports = function(req, res, next) {
  const isInvalid = Object.values(req.params).some(id => !isValid(id));

  if (isInvalid)
    return next({
      status: 400,
      message: 'Invalid ID.'
    });

  next();
};
