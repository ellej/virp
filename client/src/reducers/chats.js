import {
  ADD_CHATS, ADD_CHAT, UPDATE_CHAT, REMOVE_CHAT, SET_OPENED_CHAT,
  ADD_CHAT_MSGS, ADD_NEW_MSG, REMOVE_MSG
} from '../actions/actionTypes';

// for all active chats
export const chatsReducer = (state, action) => {
  switch (action.type) {
    case ADD_CHATS:
      return action.chats;
    case ADD_CHAT:
      return chatExists(action.chat, state)
        ? state
        : [ ...state, action.chat ];
    case UPDATE_CHAT:
      return state.map(chat => idsMatch(chat._id, action.id)
          ? { ...chat, ...action.updates }
          : chat
      );
    case REMOVE_CHAT:
      return state.filter(chat => !idsMatch(chat._id, action.id));
    default:
      throw new Error(`Unhandled type: ${action.type}`);
  }
};

// for the chat that is currently opened
export const openedChatReducer = (state, action) => {
  switch (action.type) {
    case SET_OPENED_CHAT:
      if (!action.chat)
        return action.chat; // when user is closing the opened chat (by setting it to an empty string)

      const chatIsAlreadyOpen = state._id && idsMatch(state._id, action.chat._id);
      return chatIsAlreadyOpen ? state : action.chat;
    case ADD_CHAT_MSGS:
      return { ...state, messages: action.msgs };
    case ADD_NEW_MSG:
      // cannot use the idsMatch function (apparently cannot
      // use toString on these IDs)
      if (state._id !== action.chatId)
        return state;

      const updatedMsgs = [ ...state.messages, action.newMsg ];
      return { ...state, messages: updatedMsgs };
    case REMOVE_MSG:
      const remainingMsgs = state.messages.filter(msg => !idsMatch(msg._id, action.msgId));
      return { ...state, messages: remainingMsgs };
    default:
      throw new Error(`Unhandled type: ${action.type}`);
  }
}

const idsMatch = (id1, id2) => id1.toString() === id2.toString();
const chatExists = (chat, list) => list.some(elem => idsMatch(elem._id, chat._id));
