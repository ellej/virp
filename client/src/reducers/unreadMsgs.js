import { ADD_UNREAD_MSGS, ADD_UNREAD_MSG, MARK_AS_READ } from '../actions/actionTypes';

const reducer = (state, action) => {
  switch (action.type) {
    case ADD_UNREAD_MSGS:
      return action.unreadMsgs;
    case ADD_UNREAD_MSG:
      let updatedState;
      let correspondingChat = state.filter(item => idsMatch(item.chatId, action.chatId))[0];

      if (correspondingChat) {
        const idExists = correspondingChat.unreadMsgIds.filter(id => idsMatch(id, action.unreadMsgId))[0];
        if (!idExists)
          correspondingChat.unreadMsgIds.push(action.unreadMsgId);

        updatedState = state.map(item => (
          idsMatch(item.chatId, correspondingChat.chatId))
            ? correspondingChat
            : item
        );
      }
      else {
        correspondingChat = {
          chatId: action.chatId,
          unreadMsgIds: [ action.unreadMsgId ]
        };
        updatedState = [ ...state, correspondingChat];
      }

      return updatedState;
    case MARK_AS_READ:
      return state.filter(item => !idsMatch(item.chatId, action.chatId));
    default:
      throw new Error(`Unhandled type: ${action.type}`);
  }
};

const idsMatch = (id1, id2) => id1.toString() === id2.toString();

export default reducer;
