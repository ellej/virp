import { SET_CURRENT_USER, UPDATE_USER } from '../actions/actionTypes';

const reducer = (state, action) => {
  switch (action.type) {
    case SET_CURRENT_USER:
      return action.user;
    case UPDATE_USER:
      return { ...state, ...action.updates };
    default:
      throw new Error(`Unhandled type: ${action.type}`);
  }
};

export default reducer;
