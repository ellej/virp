import JSEncrypt from 'jsencrypt';
import CryptoJS from 'crypto-js';

const crypt = new JSEncrypt({ default_key_size: 2056 });

function useCryptography(defaultPrivKey) {
  const initCrypto = (passphrase) => {
    const { pubKey, privKey } = generateKeyPair();
    const salt = generateRandomBytes();
    const symKey = generateSymKey(passphrase, salt);

    const encrPrivKey = symEncrypt(privKey, symKey);

    return {
      pubKey,
      privKey,
      encrPrivKey,
      salt
    };
  };


  // ======================= SYMMETRIC ENCRYPTION =======================

  const generateSymKey = (passphrase, salt) => hash(passphrase + salt);
  
  /*const symEncrypt = (data, symKey) => {
    const encryptedData = CryptoJS.AES.encrypt(data, symKey);
    
    return encryptedData.toString();
  };*/

  const symEncrypt = (data, symKey) => {
    const keyUtf = CryptoJS.enc.Utf8.parse(symKey);
    const iv = CryptoJS.enc.Base64.parse(symKey);
    const encryptedData = CryptoJS.AES.encrypt(data, keyUtf, { iv });
    
    return encryptedData.toString();
  };

  /*const symDecrypt = (data, symKey) => {
    const parsedData = CryptoJS.enc.Utf8.parse(data);
    const decryptedData = CryptoJS.AES.decrypt(parsedData, symKey);

    return decryptedData.toString(CryptoJS.enc.Utf8);
  };*/

  const symDecrypt = (data, symKey) => {
    let decryptedData;
    let decryptedDataStr;
    try {
      const keyUtf = CryptoJS.enc.Utf8.parse(symKey);
      const iv = CryptoJS.enc.Base64.parse(symKey);
      const parsedData = CryptoJS.enc.Base64.parse(data);
      decryptedData = CryptoJS.AES.decrypt(
        { ciphertext: parsedData },
        keyUtf,
        { iv }
      );
      decryptedDataStr = CryptoJS.enc.Utf8.stringify(decryptedData);
    }
    catch (err) {
      console.warn('could not decrypt');
      return { error: err };
    }

    return decryptedDataStr;
  };

  const generateRandomBytes = () => {
    const salt = CryptoJS.lib.WordArray.random(128 / 8);

    return salt.toString(CryptoJS.enc.Hex);
  };

  const hash = (data) => {
    const hash = CryptoJS.SHA256(data);

    return hash.toString(CryptoJS.enc.Hex);
  };


  // ======================= ASYMMETRIC ENCRYPTION =======================

  const generateKeyPair = () => {
    const privKey = crypt.getPrivateKey();
    const pubKey = crypt.getPublicKey();

    return { pubKey, privKey };
  };

  const pubEncrypt = (data, pubKey) => {
    crypt.setKey(pubKey);

    return crypt.encrypt(data).toString();
  };

  const privDecrypt = (data, privKey) => {
    crypt.setKey(privKey);

    const decrypted = crypt.decrypt(data);

    return decrypted;
  };

  return {
    initCrypto,
    generateSymKey,
    symDecrypt,
    pubEncrypt,
    privDecrypt
  };
}

export default useCryptography;
