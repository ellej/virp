import { useState, useEffect, useContext } from 'react';
import { io } from 'socket.io-client';
import { v4 as uuidv4 } from 'uuid';
import { UserContext, UserDispatchContext } from '../contexts/user';
import { ChatsDispatchContext, OpenedChatDispatchContext } from '../contexts/chats';
import { CryptoSetContext } from '../contexts/crypto';
import { ContactsDispatchContext, ContactsSearchDispatchContext } from '../contexts/contacts';
import { UnreadMsgsDispatchContext } from '../contexts/unreadMsgs';
import { ToastsDispatchContext } from '../contexts/toasts';
import { logout } from '../actions/auth';
import { updateUser } from '../actions/user';
import { addChats, addChat, updateChat, addChatMsgs, addNewMsg, setOpenedChat } from '../actions/chats';
import { addContacts, updateContact, addSearchRes } from '../actions/contacts';
import { addUnreadMsgs, addUnreadMsg } from '../actions/unreadMsgs';
import { addToast } from '../actions/toasts';
import { TOAST_NEW_MESSAGE, TOAST_ERROR } from '../utils/toastTypes';
import { getToken } from '../utils/auth';
import {
  // 'on' events
  NEW_CHAT,NEW_MESSAGE, LOADED_CHATS, LOADED_CHAT_MESSAGES, RETRIEVED_CRYPTO_DATA,
  LOADED_CONTACTS, CONTACT_SEARCH_RES, BLOCKED_BY_CONTACT, LOADED_UNREAD_MESSAGES,
  LOADED_SETTINGS, SUCCESS_DELETE_ACCOUNT, ERROR,
  // 'emit' events
  USER_CONNECTED, USER_DISCONNECTED, START_CHAT, SET_MESSAGE_DESTRUCTION,
  SEND_MESSAGE, LOAD_CHAT_MESSAGES, STORE_CRYPTO_DATA, GET_CRYPTO_DATA,
  LOAD_CONTACTS, STORE_CONTACT, GET_CONTACT_SEARCH_RES, BLOCK_CONTACT, UNBLOCK_CONTACT,
  DELETE_CONTACT, DELETE_CHAT, LOAD_UNREAD_MESSAGES, MARK_AS_READ, LOAD_SETTINGS,
  UPDATE_DEFAULT_DESTRUCTION, UPDATE_DECRYPT_BY_DEFAULT, UPDATE_NEW_MESSAGE_NOTIFICATION,
  DELETE_ACCOUNT
} from '../utils/socket';

function useWebSocket(url) {
  const [ socket ] = useState(() => io(url));
  const user = useContext(UserContext);
  const userDispatch = useContext(UserDispatchContext);
  const chatsDispatch = useContext(ChatsDispatchContext);
  const openedChatDispatch = useContext(OpenedChatDispatchContext);
  const setCryptoData = useContext(CryptoSetContext);
  const contactsDispatch = useContext(ContactsDispatchContext);
  const contactsSearchDispatch = useContext(ContactsSearchDispatchContext);
  const unreadMsgsDispatch = useContext(UnreadMsgsDispatchContext);
  const toastsDispatch = useContext(ToastsDispatchContext);

  useEffect(() => {
    socket.on('connect', () => sendConnecting());

    socket.on('disconnect', () => {});

    socket.on(ERROR, (error) => {
      const toastId = uuidv4();
      const toastType = TOAST_ERROR;
      const toastMsg = error.message;
      addToast(toastId, toastType, toastMsg, toastsDispatch);
    });

    socket.on(RETRIEVED_CRYPTO_DATA, (data) => setCryptoData({
        ...data,
        privKeyIsDecrypted: false
      })
    );

    socket.on(LOADED_SETTINGS, (settings) => updateUser(settings, userDispatch));

    socket.on(NEW_CHAT, (chat) => {
      addChat(chat, chatsDispatch);
      setOpenedChat(chat, openedChatDispatch);
    });

    socket.on(LOADED_CHATS, (chats) => {
      addChats(chats, chatsDispatch);
      loadUnreadMsgs(chats);
    });

    socket.on(LOADED_UNREAD_MESSAGES, (unreadMsgs) => {
      addUnreadMsgs(unreadMsgs, unreadMsgsDispatch);
    });

    socket.on(NEW_MESSAGE, ({ msg, chat }) => {
      const chatId = chat._id.toString();

      addChat(chat, chatsDispatch);
      addNewMsg(msg, chatId, openedChatDispatch);
      updateChat(chatId, { mostRecentMsgDate: chat.mostRecentMsgDate }, chatsDispatch);

      const userIsReceiver = msg.receiver.toString() === user._id.toString();
      if (userIsReceiver) {
        const toastId = uuidv4();
        const toastType = TOAST_NEW_MESSAGE;
        const toastMsg = 'New message received!';
        addToast(toastId, toastType, toastMsg, toastsDispatch);
        addUnreadMsg(msg._id, chat._id, unreadMsgsDispatch);
      }
    });

    socket.on(LOADED_CHAT_MESSAGES, (msgs) => addChatMsgs(msgs, openedChatDispatch));

    socket.on(LOADED_CONTACTS, (contacts) => addContacts(contacts, contactsDispatch));

    socket.on(CONTACT_SEARCH_RES, (res) => addSearchRes(res, contactsSearchDispatch));

    socket.on(BLOCKED_BY_CONTACT, ({ contactId, blockedByContact }) => {
      updateContact(contactId, { blockedByContact }, contactsDispatch);
    });

    socket.on(SUCCESS_DELETE_ACCOUNT, () => logout(userDispatch));

    // Remove all listeners to prevent adding duplicate listeners which causes
    // them to get fired multiple times.
    return () => socket.removeAllListeners();
  }, []);

  const emit = (event, data) => {
    switch (event) {
      case USER_CONNECTED:
        sendConnecting();
        break;
      case USER_DISCONNECTED:
        sendDisconnecting();
        break;
      case START_CHAT:
        startChat(data);
        break;
      case SEND_MESSAGE:
        sendMessage(data);
        break;
      case SET_MESSAGE_DESTRUCTION:
        setMsgDestruction(data);
        break;
      case LOAD_CHAT_MESSAGES:
        loadChatMsgs(data);
        break;
      case STORE_CRYPTO_DATA:
        storeCryptoData(data);
        break;
      case GET_CRYPTO_DATA:
        getCryptoData(data);
        break;
      case LOAD_CONTACTS:
        loadContacts(data);
        break;
      case STORE_CONTACT:
        storeContact(data);
        break;
      case GET_CONTACT_SEARCH_RES:
        getContactSearchRes(data);
        break;
      case BLOCK_CONTACT:
        blockContact(data);
        break;
      case UNBLOCK_CONTACT:
        unblockContact(data);
        break;
      case DELETE_CONTACT:
        deleteContact(data);
        break;
      case DELETE_CHAT:
        deleteChat(data);
        break;
      case MARK_AS_READ:
        markAsRead(data);
        break;
      case LOAD_SETTINGS:
        loadSettings(data);
        break;
      case UPDATE_DEFAULT_DESTRUCTION:
        updateDefaultMsgDestruction(data);
        break;
      case UPDATE_DECRYPT_BY_DEFAULT:
        updateDecryptMsgByDefault(data);
        break;
      case UPDATE_NEW_MESSAGE_NOTIFICATION:
        updateNewMsgNotification(data);
        break;
      case DELETE_ACCOUNT:
        deleteAccount(data);
        break;
      default:
        throw new Error(`Unhandled event: ${event}`);
    }
  };

  const sendConnecting = () => {
    // server expects: userId = payload
    socket.emit(USER_CONNECTED, user._id);
    const userUpdates = { socketId: socket.id };
    updateUser(userUpdates, userDispatch);
  };

  const sendDisconnecting = () => {
    // server expects: userId = payload
    socket.emit(USER_DISCONNECTED, user._id);
  };

  const loadUnreadMsgs = (chats) => {
    // server expects: { userId, chats } = payload
    socket.emit(LOAD_UNREAD_MESSAGES, { userId: user._id, chats });
  };

  const startChat = (receiverId) => {
    // server expects: { senderId, receiverId } = payload
    socket.emit(START_CHAT, { senderId: user._id, receiverId });
  };

  const sendMessage = (data) => {
    // server expects:
    // { chatId, sender, receiver, msgEncryptedForSender, msgEncryptedForReceiver, pubKey } = payload
    socket.emit(SEND_MESSAGE, { ...data, sender: user });
  };

  const setMsgDestruction = (data) => {
    // server expects: { chatId, userId, destructionOpt } = payload
    socket.emit(SET_MESSAGE_DESTRUCTION, { ...data, userId: user._id });
  };

  const loadChatMsgs = (chatId) => {
    // server expects: chatId = payload
    socket.emit(LOAD_CHAT_MESSAGES, chatId);
  };

  const storeCryptoData = (data) => {
    // server expects: { userId, pubKey, encrPrivKey, salt } = payload
    socket.emit(STORE_CRYPTO_DATA, { ...data, userId: user });
  };

  const getCryptoData = () => {
    // server expects: user = payload
    socket.emit(GET_CRYPTO_DATA, user._id);
  };

  const loadContacts = () => {
    // server expects: userId = payload
    socket.emit(LOAD_CONTACTS, user._id);
  };

  const storeContact = (contactId) => {
    // server expects: { userId, contactId } = payload
    socket.emit(STORE_CONTACT, { userId: user._id, contactId });
  };

  const getContactSearchRes = (usernamePrefix) => {
    // server expects: { usernamePrefix, searcherId } = payload
    socket.emit(GET_CONTACT_SEARCH_RES, { usernamePrefix, searcherId: user._id });
  };

  const blockContact = (contactId) => {
    // server expects: { userId, contactId } = payload
    socket.emit(BLOCK_CONTACT, { userId: user._id, contactId });
  }

  const unblockContact = (contactId) => {
    // server expects: { userId, contactId } = payload
    socket.emit(UNBLOCK_CONTACT, { userId: user._id, contactId });
  }

  const deleteContact = (contactId) => {
    // server expects: { userId, contactId } = payload
    socket.emit(DELETE_CONTACT, { userId: user._id, contactId });
  };

  const deleteChat = (chatId) => {
    // server expects: { userId, chatId } = payload
    socket.emit(DELETE_CHAT, { userId: user._id, chatId });
  };

  const markAsRead = (unreadMsgIds) => {
    // server expects: { unreadMsgIds, userId } = payload
    socket.emit(MARK_AS_READ, { unreadMsgIds, userId: user._id });
  };

  const loadSettings = () => {
    // server expects: userId = payload
    socket.emit(LOAD_SETTINGS, user._id);
  };

  const updateDefaultMsgDestruction = (data) => {
    // server expects: { userId, defaultMsgDestruction } = payload
    socket.emit(UPDATE_DEFAULT_DESTRUCTION, { userId: user._id, defaultMsgDestruction: data });
  };

  const updateDecryptMsgByDefault = (data) => {
    // server expects: { userId, decryptMsgByDefault } = payload
    socket.emit(UPDATE_DECRYPT_BY_DEFAULT, { userId: user._id, decryptMsgByDefault: data });
  };

  const updateNewMsgNotification = (data) => {
    // server expects: { userId, newMsgNotificationOn } = payload
    socket.emit(UPDATE_NEW_MESSAGE_NOTIFICATION, { userId: user._id, newMsgNotificationOn: data });
  };

  const deleteAccount = (password) => {
    // server expects: { userId, password, token } = payload
    socket.emit(DELETE_ACCOUNT, { userId: user._id, password, token: getToken() });
  };

  return [ emit ];
}

export default useWebSocket;
