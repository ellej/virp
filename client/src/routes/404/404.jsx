import { Link } from 'react-router-dom';
import { createUseStyles } from 'react-jss';
import { motion } from 'framer-motion';
import Header from '../../components/Header/Header';
import { ROUTE_HOME } from '../../utils/routePaths';
import commonStyles from '../../utils/commonStyles';
import styles from './404.styles';

const useStyles = createUseStyles(styles);

function Route404() {
  const classes = useStyles();

  return (
    <div className={classes.Route404}>
      <Header />
      <motion.div
        initial={commonStyles.pageTransitionVar.initial}
        animate={commonStyles.pageTransitionVar.animate}
        exit={commonStyles.pageTransitionVar.exit}
        variants={commonStyles.pageTransition.fade}
        className={classes.content}
      >
        <div className={classes.infoBox}>
          <p className={classes.infoBoxTitle}>404 not found!</p>
          <p>Woops... looks like you{"'"}ve hit a dead end {":'("}</p>
        </div>
        <div className={classes.backBtn}>
          <Link to={ROUTE_HOME}>&#171; take me back home</Link>
        </div>
      </motion.div>
    </div>
  );
}

export default Route404;
