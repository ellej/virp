import commonStyles from '../../utils/commonStyles';

const styles = {
  Route404: commonStyles.fullViewFlexColumn,
  content: commonStyles.fullWidthContentCentered,
  infoBox: commonStyles.infoBox,
  infoBoxTitle: commonStyles.infoBoxTitle,
  backBtn: {
    marginTop: '100px',
    '& a': {
      color: commonStyles.brandGoldMain
    }
  }
};

export default styles;
