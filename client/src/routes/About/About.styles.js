import commonStyles from '../../utils/commonStyles';

const styles = {
  About: commonStyles.fullViewFlexColumn,
  content: commonStyles.fullWidthContentCentered,
  infoBox: {
    ...commonStyles.infoBox,
    marginBottom: '70px'
  },
  infoBoxTitle: commonStyles.infoBoxTitle,
  infoBoxInnerTitle: {
    fontWeight: '700'
  },
  backBtn: commonStyles.backBtn
};

export default styles;
