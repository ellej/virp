import commonStyles from '../../utils/commonStyles';

const styles = {
  Dashboard: {
    width: '100vw',
    height: '100vh',
    display: 'flex',
    backgroundColor: commonStyles.brandDarkMain,
    overflow: 'hidden'
  },
  loading: {
    position: 'absolute',
    top: '0',
    left: '0',
    width: '100vw',
    height: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: commonStyles.brandDarkMain,
    zIndex: '100'
  }
};

export default styles;
