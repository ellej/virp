import { Link } from 'react-router-dom';
import { createUseStyles } from 'react-jss';
import { motion } from 'framer-motion';
import { ROUTE_SIGNUP, ROUTE_LOGIN } from '../../utils/routePaths';
import Header from '../../components/Header/Header';
import commonStyles from '../../utils/commonStyles';
import styles from './Homepage.styles';

const useStyles = createUseStyles(styles);

function Homepage() {
  const classes = useStyles();

  return (
    <div className={classes.Homepage}>
      <Header />
      <motion.div
        initial={commonStyles.pageTransitionVar.initial}
        animate={commonStyles.pageTransitionVar.animate}
        exit={commonStyles.pageTransitionVar.exit}
        variants={commonStyles.pageTransition.upToDown}
        className={classes.content}
      >
        <div className={classes.buttons}>
          <Link to={ROUTE_LOGIN}>
            <div className={classes.button}>
              LOG IN
            </div>
          </Link>
          <Link to={ROUTE_SIGNUP}>
            <div className={classes.button}>
              SIGN UP
            </div>
          </Link>
        </div>
      </motion.div>
    </div>
  );
}

export default Homepage;
