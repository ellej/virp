import commonStyles from '../../utils/commonStyles';
import mediaQuerySizes from '../../utils/mediaQuerySizes';

const styles = {
  AuthForm: commonStyles.fullViewFlexColumn,
  content: {
    ...commonStyles.fullWidthContentCentered,
    marginBottom: '60px',
    '& form': {
      width: '50%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      '& input': {
        width: '75%',
        padding: '30px 15px',
        fontSize: '20px',
        textAlign: 'center',
        backgroundColor: 'transparent',
        border: 'none',
        borderBottom: '2px solid white',
        transition: 'width 200ms ease-in',
        '&:focus': {
          width: '100%',
          outline: 'none'
        },
        '&:required': {
          boxShadow: 'none'
        },
        [mediaQuerySizes.maxWidth.md()]: {
          fontSize: '18px'
        },
        [mediaQuerySizes.maxWidth.sm()]: {
          fontSize: '16px'
        },
        [mediaQuerySizes.maxHeightLaptop.lg()]: {
          padding: '22px 15px',
          fontSize: '18px'
        }
      },
      '& button': {
        ...commonStyles.btnLarge,
        margin: '80px 0 40px',
        [mediaQuerySizes.maxHeight.xl()]: {
          margin: '50px 0 40px'
        },
        [mediaQuerySizes.maxHeightLaptop.md()]: {
          width: '220px',
          height: '120px',
          fontSize: '38px',
          '&:hover': {
            width: '260px'
          }
        }
      },
      [mediaQuerySizes.maxWidth.md()]: {
        width: '60%'
      },
      [mediaQuerySizes.maxWidth.sm()]: {
        width: '80%'
      }
    },
    [mediaQuerySizes.maxHeightLaptop.md()]: {
      marginTop: '-50px'
    }
  },
  formRow: {
    width: '100%',
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '30px',
    '& span': {
      position: 'absolute',
      top: '50%',
      left: '2%',
      transform: 'translateY(-50%)',
      width: '50px',
      height: '50px',
      marginRight: '5px',
      marginBottom: '20px',
      backgroundSize: '100%',
      borderRadius: '50%',
      transition: 'all 100ms ease-in-out',
      '&:hover': {
        transform: 'translateY(-60%)'
      },
      [mediaQuerySizes.maxWidth.sm()]: {
        width: '45px',
        height: '45px'
      },
      [mediaQuerySizes.maxWidth.xs()]: {
        width: '40px',
        height: '40px'
      },
      [mediaQuerySizes.maxHeightLaptop.md()]: {
        width: '45px',
        height: '45px'
      }
    },
    [mediaQuerySizes.maxHeight.xl()]: {
      marginTop: '15px'
    }
  },
  iconUser: {
    background: commonStyles.iconLoginProfileWhite
  },
  iconPassword: {
    background: commonStyles.iconProtectedWhite
  },
  inputErrorMessage: {
    width: '100%',
    height: '15px',
    position: 'absolute',
    bottom: '-25px',
    left: 0,
    textAlign: 'center',
    fontSize: '12px',
    color: '#c52020',
    [mediaQuerySizes.maxWidth.xs()]: {
      fontSize: '10px'
    }
  },
  footer: {
    '& span': {
      cursor: 'pointer'
    },
    [mediaQuerySizes.maxWidth.sm()]: {
      fontSize: '14px'
    },
    [mediaQuerySizes.maxHeightLaptop.md()]: {
      fontSize: '14px'
    }
  },
  infoBox: {
    ...commonStyles.infoBox,
    [mediaQuerySizes.maxWidth.md()]: {
      marginBottom: '70px'
    }
  },
  infoBoxTitle: commonStyles.infoBoxTitle,
  backBtn: commonStyles.backBtn,
  /* --------- start: hidden field for detecting spam: --------- */
  email: {
    display: 'none !important'
  }
  /* -------------------- end -------------------- */
};

export default styles;
