import { createContext } from 'react';
import useWebSocket from '../hooks/useWebSocket';
import { SERVER_URL } from '../config/urls';

export const SocketEmitContext = createContext();

export function SocketProvider(props) {
  const [ emit ] = useWebSocket(SERVER_URL);

  return (
    <SocketEmitContext.Provider value={emit}>
      {props.children}
    </SocketEmitContext.Provider>
  );
}
