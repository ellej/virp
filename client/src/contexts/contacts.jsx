import { useReducer, createContext } from 'react';
import { contactsReducer, searchResReducer } from '../reducers/contacts';

export const ContactsContext = createContext();
export const ContactsDispatchContext = createContext();
export const ContactsSearchContext = createContext();
export const ContactsSearchDispatchContext = createContext();

const defaultContacts = null;
const defaultSearchRes = null;

export function ContactsProvider(props) {
  const [ contacts, contactsDispatch ] = useReducer(contactsReducer, defaultContacts);
  const [ searchRes, searchResDispatch ] = useReducer(searchResReducer, defaultSearchRes);

  return (
    <ContactsContext.Provider value={contacts}>
      <ContactsDispatchContext.Provider value={contactsDispatch}>
        <ContactsSearchContext.Provider value={searchRes}>
          <ContactsSearchDispatchContext.Provider value={searchResDispatch}>
            {props.children}
          </ContactsSearchDispatchContext.Provider>
        </ContactsSearchContext.Provider>
      </ContactsDispatchContext.Provider>
    </ContactsContext.Provider>
  );
}
