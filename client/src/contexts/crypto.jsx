import { useState, createContext } from 'react';

export const CryptoContext = createContext();
export const CryptoSetContext = createContext();

export function CryptoProvider(props) {
  // crypto data will store { privKey, salt, privKeyIsDecrypted }
  const [ cryptoData, setCryptoData ] = useState('');

  return (
    <CryptoContext.Provider value={cryptoData}>
      <CryptoSetContext.Provider value={setCryptoData}>
        {props.children}
      </CryptoSetContext.Provider>
    </CryptoContext.Provider>
  );
}
