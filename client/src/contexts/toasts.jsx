import { useReducer, createContext } from 'react';
import reducer from '../reducers/toasts';

export const ToastsContext = createContext();
export const ToastsDispatchContext = createContext();

const defaultToasts = [];

export function ToastsProvider(props) {
  const [ toasts, dispatch ] = useReducer(reducer, defaultToasts);

  return (
    <ToastsContext.Provider value={toasts}>
      <ToastsDispatchContext.Provider value={dispatch}>
        {props.children}
      </ToastsDispatchContext.Provider>
    </ToastsContext.Provider>
  );
}
