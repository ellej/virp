import { useReducer, createContext } from 'react';
import reducer from '../reducers/unreadMsgs';

export const UnreadMsgsContext = createContext();
export const UnreadMsgsDispatchContext = createContext();

const defaultUnreadMsgs = [];

export function UnreadMsgsProvider(props) {
  const [ unreadMsgs, dispatch ] = useReducer(reducer, defaultUnreadMsgs);

  return (
    <UnreadMsgsContext.Provider value={unreadMsgs}>
      <UnreadMsgsDispatchContext.Provider value={dispatch}>
        {props.children}
      </UnreadMsgsDispatchContext.Provider>
    </UnreadMsgsContext.Provider>
  );
}
