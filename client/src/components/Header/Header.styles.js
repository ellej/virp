import commonStyles from '../../utils/commonStyles';
import mediaQuerySizes from '../../utils/mediaQuerySizes';

const styles = {
  Header: {
    width: '100vw',
    minHeight: commonStyles.headerHeight,
    height: commonStyles.headerHeight,
    padding: '0 40px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    '& a': {
      display: 'flex'
    }
  },
  item: {
    width: '115px',
    display: 'flex',
    fontWeight: '600',
    fontSize: '14px',
    justifyContent: 'center',
    alignItems: 'center',
    color: commonStyles.brandGoldMain,
    transition: 'all 120ms ease-in-out',
    '&:hover': {
      letterSpacing: '3px'
    },
    [mediaQuerySizes.maxWidth.xs()]: {
      fontSize: '12px'
    }
  }
};

export default styles;
