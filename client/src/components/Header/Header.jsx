import { Link } from 'react-router-dom';
import { createUseStyles } from 'react-jss';
import { ROUTE_HOME, ROUTE_ABOUT } from '../../utils/routePaths';
import styles from './Header.styles';

const useStyles = createUseStyles(styles);

function Header() {
  const classes = useStyles();

  return (
    <div className={classes.Header}>
      <Link to={ROUTE_HOME}>
        <div className={classes.item}>
          &#171; VIRP &#187;
        </div>
      </Link>
      <Link to={ROUTE_ABOUT}>
        <div className={classes.item}>
          &#171; ABOUT &#187;
        </div>
      </Link>
    </div>
  );
}

export default Header;
