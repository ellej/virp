import commonStyles from '../../../utils/commonStyles';

const styles = {
  AboutBox: {
    ...commonStyles.mainWindowContentBox,
    justifyContent: 'center'
  },
  content: {
    width: '100%',
    height: `calc(100% - ${commonStyles.mainWindowContentBoxHeader.height})`,
    padding: '30px 60px',
    textAlign: 'center',
    fontSize: '14px',
    overflowY: 'auto',
    scrollbarWidth: 'thin', // firefox
    '& p': {
      margin: '30px 0',
      lineHeight: '2em'
    }
  },
  innerTitle: {
    color: commonStyles.brandGoldMain
  }
};

export default styles;
