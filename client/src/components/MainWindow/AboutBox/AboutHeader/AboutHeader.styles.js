import commonStyles from '../../../../utils/commonStyles';

const styles = {
  AboutHeader: commonStyles.mainWindowContentBoxHeader,
  title: commonStyles.mainWindowContentBoxTitle,
  closeBtn: commonStyles.mainWindowContentBoxCloseBtn
};

export default styles;
