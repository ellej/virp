import { motion } from 'framer-motion';
import { createUseStyles } from 'react-jss';
import AboutHeader from './AboutHeader/AboutHeader';
import commonStyles from '../../../utils/commonStyles';
import styles from './AboutBox.styles';

const useStyles = createUseStyles(styles);

function AboutBox({ close }) {
  const classes = useStyles();

  return (
    <motion.div
      initial={commonStyles.pageTransitionVar.initial}
      animate={commonStyles.pageTransitionVar.animate}
      exit={commonStyles.pageTransitionVar.exit}
      variants={commonStyles.pageTransition.horizontal}
      transition={commonStyles.customTransition.tween}
      className={classes.AboutBox}
    >
      <AboutHeader close={close} />
      <div className={classes.content}>
        <p className={classes.innerTitle}>What We Do</p>
        <p>
          Virp is a messaging application that provides a seamless way for people to
          securely and privately communicate with each other using end-to-end encryption.
          We are firm believers of privacy and our goal is to increase the
          awareness of the importance of protecting and respecting people's data, as well
          as to keep providing privacy oriented applications.
        </p>
        <p className={classes.innerTitle}>End-to-End Encryption</p>
        <p>
          All messages sent are end-to-end encrypted by default. In short, this means
          that only the intended receiver can decrypt it. One could think of this as
          the recipient being the only one with the key to unlock the message. Accordingly,
          if a message was to be intercepted, the actual contents would be unreadable.
          We at Virp are also unable to see any decrypted messages.
        </p>
        <p className={classes.innerTitle}>No Permanent Records</p>
        <p>
          By default, messages self-destruct after 30 days, thus no messages are ever
          kept beyond that time period. This is due to that we want to provide a platform
          where people can feel confident in the fact that their data will not be stored
          for an unnecessary amount of time. Self-destruction timers for each individual
          message may, however, be set to shorter time frames by the user.
        </p>
        <p>
          We're glad to have you join the Virp family!
        </p>
      </div>
    </motion.div>
  );
}

export default AboutBox;
