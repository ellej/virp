import { createUseStyles } from 'react-jss';
import styles from './NewMsgBtn.styles';

const useStyles = createUseStyles(styles);

function NewMsgBtn({ onClick }) {
  const classes = useStyles();

  return (
    <button
      className={classes.newMsgBtn}
      onClick={onClick}
      type='button'
    />
  );
}

export default NewMsgBtn;
