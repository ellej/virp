import commonStyles from '../../../utils/commonStyles';
import mediaQuerySizes from '../../../utils/mediaQuerySizes';

const styles = {
  newMsgBtn: {
    width: '80px',
    height: '80px',
    position: 'absolute',
    bottom: '40px',
    right: '3%',
    padding: '10px',
    outline: 'none',
    border: `2px solid ${commonStyles.brandGoldMain}`,
    borderRadius: '50%',
    boxShadow: commonStyles.boxShadowOutGoldSmall,
    background: commonStyles.iconNewMsgGreen,
    backgroundSize: '60%',
    cursor: 'pointer',
    transition: 'all 150ms ease-in-out',
    '&:focus': {
      outline: 'none'
    },
    '&:hover': {
      boxShadow: commonStyles.boxShadowOutGoldMedium,
      transform: 'translateY(-5px)'
    },
    [mediaQuerySizes.maxWidth.lg()]: {
      right: '2%',
      bottom: '20px'
    },
    [mediaQuerySizes.maxHeightLaptop.lg()]: {
      right: '2%',
      bottom: '20px'
    }
  }
};

export default styles;
