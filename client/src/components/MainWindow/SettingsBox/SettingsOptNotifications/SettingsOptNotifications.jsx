import { createUseStyles } from 'react-jss';
import SettingsOpt from '../SettingsOpt/SettingsOpt';
import ToggleBtn from '../../../ToggleBtn/ToggleBtn';
import { UPDATE_NEW_MESSAGE_NOTIFICATION } from '../../../../utils/socket';
import styles from './SettingsOptNotifications.styles';

const useStyles = createUseStyles(styles);

function SettingsOptNotifications({ newMsgNotificationOn, userDispatch, updateUser, emit }) {

  const toggleNewMsg = () => {
    updateUser({ newMsgNotificationOn: !newMsgNotificationOn }, userDispatch);
    emit(UPDATE_NEW_MESSAGE_NOTIFICATION, !newMsgNotificationOn);
  };

  const classes = useStyles();

  return (
    <SettingsOpt title="notifications settings">
      <div className={classes.subheading}>
        notify me on:
      </div>
      <div className={classes.notificationsSettingsItem}>
        <span>new message</span>
        <ToggleBtn
          isOn={newMsgNotificationOn}
          toggle={toggleNewMsg}
        />
      </div>
    </SettingsOpt>
  );
}

export default SettingsOptNotifications;
