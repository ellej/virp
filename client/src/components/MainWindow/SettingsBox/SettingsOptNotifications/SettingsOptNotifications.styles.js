import commonStyles from '../../../../utils/commonStyles';

const styles = {
  notificationsSettingsItem: commonStyles.settingsOptItem,
  subheading: {
    width: '100%',
    marginTop: '53px',
    fontSize: '12px',
    color: commonStyles.noisyWhite
  }
};

export default styles;
