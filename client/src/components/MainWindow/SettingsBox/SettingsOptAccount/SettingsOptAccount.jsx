import { useState } from 'react';
import { createUseStyles } from 'react-jss';
import Moment from 'react-moment';
import SettingsOpt from '../SettingsOpt/SettingsOpt';
import Confirmation from '../../../Confirmation/Confirmation';
import iconTrashRed from '../../../../assets/iconTrashRed.png';
import { DELETE_ACCOUNT } from '../../../../utils/socket';
import styles from './SettingsOptAccount.styles';

const useStyles = createUseStyles(styles);

function SettingsOptAccount({ username, memberSince, emit }) {
  const [ confirmationIsOpen, setConfirmationIsOpen ] = useState(false);

  const requestDeleteAccount = (password) => {
    if (!password)
      return;

    emit(DELETE_ACCOUNT, password);
  };

  const openConfirmation = () => setConfirmationIsOpen(true);
  const closeConfirmation = () => setConfirmationIsOpen(false);

  const classes = useStyles();

  return (
    <SettingsOpt title="account settings">
      <div className={classes.accountSettingsItem}>
        <span>username</span>
        <span className={`${classes.value} ${classes.valueUsername}`}>
          {username}
        </span>
      </div>
      <div className={classes.accountSettingsItem}>
        <span>password</span>
        <span className={`${classes.value} ${classes.valuePassword}`}>
          change password
        </span>
      </div>
      <div className={classes.accountSettingsItem}>
        <span>member</span>
        <span className={`${classes.value} ${classes.valueMember}`}>
          since&nbsp;
          <Moment format="MMM D YYYY">
            {memberSince}
          </Moment>
        </span>
      </div>
      <div className={classes.accountSettingsItem}>
        <span>delete account</span>
        <img
          className={classes.deleteBtn}
          src={iconTrashRed}
          alt='delete account'
          onClick={openConfirmation}
        />
      </div>
      {confirmationIsOpen && (
        <Confirmation
          title="We're sad to see you go.. :("
          message="Please confirm that you wish to permanently delete your account."
          confirm={requestDeleteAccount}
          cancel={closeConfirmation}
          confirmText="DELETE"
          passwordRequired={true}
        />
      )}
    </SettingsOpt>
  );
}

export default SettingsOptAccount;
