import commonStyles from '../../../../utils/commonStyles';

const styles = {
  SettingsSidebar: {
    width: commonStyles.settingsSidebarWidth,
    height: 'max-content',
    border: `1px solid ${commonStyles.quietWhite}`
  }
};

export default styles;
