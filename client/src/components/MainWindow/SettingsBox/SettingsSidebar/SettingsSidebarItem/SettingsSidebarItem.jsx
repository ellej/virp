import { createUseStyles } from 'react-jss';
import styles from './SettingsSidebarItem.styles';

const useStyles = createUseStyles(styles);

function SettingsSidebarItem({ name, isOpen, open }) {
  const classes = useStyles({ isOpen });

  return (
    <div
      className={classes.SettingsSidebarItem}
      onClick={() => open()}
    >
      {name}
    </div>
  );
}

export default SettingsSidebarItem;
