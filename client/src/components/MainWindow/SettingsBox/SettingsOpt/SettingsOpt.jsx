import { motion } from 'framer-motion';
import { createUseStyles } from 'react-jss';
import commonStyles from '../../../../utils/commonStyles'
import styles from './SettingsOpt.styles';

const useStyles = createUseStyles(styles);

function SettingsOpt({ title, children }) {
  const classes = useStyles();

  return (
    <motion.div
      initial={commonStyles.pageTransitionVar.initial}
      animate={commonStyles.pageTransitionVar.animate}
      exit={commonStyles.pageTransitionVar.exit}
      variants={commonStyles.pageTransition.fade}
      className={classes.SettingsOpt}
    >
      <div className={classes.title}>
        {title}
      </div>
      <div className={classes.info}>
        {children}
      </div>
    </motion.div>
  );
}

export default SettingsOpt;
