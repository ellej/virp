import commonStyles from '../../../utils/commonStyles';

const styles = {
  SettingsBox: commonStyles.mainWindowContentBox,
  content: {
    width: '100%',
    height: `calc(100% - ${commonStyles.mainWindowContentBoxHeader.height})`,
    padding: '60px',
    display: 'flex',
    justifyContent: 'space-between'
  }
};

export default styles;
