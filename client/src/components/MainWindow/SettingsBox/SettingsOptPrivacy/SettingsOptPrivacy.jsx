import { createUseStyles } from 'react-jss';
import SettingsOpt from '../SettingsOpt/SettingsOpt';
import ToggleBtn from '../../../ToggleBtn/ToggleBtn';
import { expirations } from '../../../../utils/msgExpirations';
import { UPDATE_DEFAULT_DESTRUCTION, UPDATE_DECRYPT_BY_DEFAULT } from '../../../../utils/socket';
import styles from './SettingsOptPrivacy.styles';

const useStyles = createUseStyles(styles);

function SettingsOptPrivacy({ defaultMsgDestruction, decryptMsgByDefault, userDispatch, updateUser, emit }) {

  const changeDefaultMsgDestruction = (e) => {
    updateUser({ defaultMsgDestruction: e.target.value }, userDispatch);
    emit(UPDATE_DEFAULT_DESTRUCTION, e.target.value);
  };

  const toggleClickToUnlock = () => {
    updateUser({ decryptMsgByDefault: !decryptMsgByDefault }, userDispatch);
    emit(UPDATE_DECRYPT_BY_DEFAULT, !decryptMsgByDefault);
  };

  const classes = useStyles();

  return (
    <SettingsOpt title="privacy settings">
      <div className={`${classes.privacySettingsItem} ${classes.itemSelfDestruction}`}>
        <span className={classes.setting}>
          default self-destruction timer
        </span>
        <span className={classes.setting}>
          (for outgoing messages)
        </span>
        <form onSubmit={() => {}}>
          {Object.values(expirations).map(opt => (
            <label
              key={opt}
              htmlFor={opt}
            > {opt}
              <input
                type="radio"
                id={opt}
                name="self_destruction"
                value={opt}
                checked={defaultMsgDestruction === opt}
                onChange={changeDefaultMsgDestruction}
              />
              <span></span>
            </label>
          ))}
        </form>
      </div>
      <div className={classes.privacySettingsItem}>
        <span>
          automatically unlock messages
        </span>
        <ToggleBtn
          isOn={decryptMsgByDefault}
          toggle={toggleClickToUnlock}
        />
      </div>
    </SettingsOpt>
  );
}

export default SettingsOptPrivacy;
