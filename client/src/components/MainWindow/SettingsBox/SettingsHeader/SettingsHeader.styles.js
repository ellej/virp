import commonStyles from '../../../../utils/commonStyles';

const styles = {
  SettingsHeader: commonStyles.mainWindowContentBoxHeader,
  title: commonStyles.mainWindowContentBoxTitle,
  closeBtn: commonStyles.mainWindowContentBoxCloseBtn
};

export default styles;
