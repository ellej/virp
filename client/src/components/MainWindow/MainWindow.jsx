import { createUseStyles } from 'react-jss';
import { AnimatePresence } from 'framer-motion';
import ChatBox from './ChatBox/ChatBox';
import ContactsBox from './ContactsBox/ContactsBox';
import AboutBox from './AboutBox/AboutBox';
import SettingsBox from './SettingsBox/SettingsBox';
import NewMsgBtn from './NewMsgBtn/NewMsgBtn';
import { OPENED_BOX_CONTACTS, OPENED_BOX_SETTINGS, OPENED_BOX_ABOUT } from '../../utils/openedBox';
import styles from './MainWindow.styles';

const useStyles = createUseStyles(styles);

function MainWindow({ openedChat, openedBox, switchOpenedBox, closeOpenedBox }) {

  const getOpenedBox = () => {
    if (openedChat)
      return <ChatBox chat={openedChat} />;

    switch (openedBox) {
      case OPENED_BOX_CONTACTS:
        return <ContactsBox close={closeOpenedBox} />;
      case OPENED_BOX_SETTINGS:
        return <SettingsBox close={closeOpenedBox} />;
      case OPENED_BOX_ABOUT:
        return <AboutBox close={closeOpenedBox} />;
      default:
        return <></>;
    }
  };

  const classes = useStyles();

  return (
    <div className={classes.MainWindow}>
      {(openedChat || openedBox) && (
        <AnimatePresence>
          {getOpenedBox()}
        </AnimatePresence>
      )}
      <NewMsgBtn
        onClick={() => switchOpenedBox(OPENED_BOX_CONTACTS)}
      />
    </div>
  );
}

export default MainWindow;
