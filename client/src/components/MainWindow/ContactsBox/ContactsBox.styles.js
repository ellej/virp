import commonStyles from '../../../utils/commonStyles';

const styles = {
  ContactsBox: commonStyles.mainWindowContentBox
};

export default styles;
