import { useState, useContext } from 'react';
import { createUseStyles } from 'react-jss';
import { motion } from 'framer-motion';
import { ContactsContext, ContactsSearchContext } from '../../../contexts/contacts';
import { SocketEmitContext } from '../../../contexts/socket';
import ContactsHeader from './ContactsHeader/ContactsHeader';
import ContactsList from './ContactsList/ContactsList';
import commonStyles from '../../../utils/commonStyles';
import styles from './ContactsBox.styles';

const useStyles = createUseStyles(styles);

function ContactsBox({ close }) {
  const [ isSearching, setIsSearching ] = useState(false);
  const contacts = useContext(ContactsContext);
  const searchRes = useContext(ContactsSearchContext);
  const emit = useContext(SocketEmitContext);

  const classes = useStyles();

  return (
    <motion.div
      initial={commonStyles.pageTransitionVar.initial}
      animate={commonStyles.pageTransitionVar.animate}
      exit={commonStyles.pageTransitionVar.exit}
      variants={commonStyles.pageTransition.horizontal}
      transition={commonStyles.customTransition.tween}
      className={classes.ContactsBox}
    >
      <ContactsHeader
        emit={emit}
        isSearching={isSearching}
        setIsSearching={setIsSearching}
        close={close}
      />
      <ContactsList
        emit={emit}
        isSearching={isSearching}
        contacts={contacts}
        searchRes={searchRes}
      />
    </motion.div>
  );
}

export default ContactsBox;
