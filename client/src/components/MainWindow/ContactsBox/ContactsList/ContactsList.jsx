import { useContext } from 'react';
import { createUseStyles } from 'react-jss';
import { motion } from 'framer-motion';
import { ChatsContext } from '../../../../contexts/chats';
import Contact from './Contact/Contact';
import styles from './ContactsList.styles';

const useStyles = createUseStyles(styles);

function ContactsList({ emit, isSearching, contacts, searchRes }) {
  const chats = useContext(ChatsContext);

  const currentList = isSearching ? searchRes : contacts;

  const getInfoMsg = () => {
    if (!currentList)
      return '';

    const numContacts = currentList.length;

    if (isSearching) {
      let msg = 'search mode';

      if (!numContacts)
        return msg;

      msg += `: ${numContacts} result`;

      return numContacts > 1 ? msg + 's' : msg;
    }

    return `${numContacts} contacts`;
  };

  const idsMatch = (id1, id2) => id1.toString() === id2.toString();

  const classes = useStyles();

  return (
    <motion.div
      positionTransition
      className={classes.ContactsList}
    >
      <div className={classes.infoMsg}>
        {getInfoMsg()}
      </div>
      {currentList && currentList
        .sort((a, b) => a.username < b.username ? -1 : 1)
        .map(contact => (
          <Contact
            key={contact._id.toString()}
            contact={contact}
            hasBeenAdded={contacts.some(c => c.username === contact.username)}
            correspondingChat={chats.find(
              c => c.members.some(m => idsMatch(m.user, contact._id))
            )}
            emit={emit}
          />
      ))}
    </motion.div>
  );
}

export default ContactsList;
