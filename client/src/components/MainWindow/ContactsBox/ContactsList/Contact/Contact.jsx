import { useState, useContext } from 'react';
import { createUseStyles } from 'react-jss';
import Confirmation from '../../../../Confirmation/Confirmation';
import ToggleBtn from '../../../../ToggleBtn/ToggleBtn';
import { ChatsDispatchContext, OpenedChatDispatchContext } from '../../../../../contexts/chats';
import { ContactsDispatchContext } from '../../../../../contexts/contacts';
import { setOpenedChat, removeChat } from '../../../../../actions/chats';
import { addContact, updateContact, removeContact } from '../../../../../actions/contacts';
import { START_CHAT, STORE_CONTACT, BLOCK_CONTACT, UNBLOCK_CONTACT, DELETE_CONTACT } from '../../../../../utils/socket';
import iconNewMsgSmallGreen from '../../../../../assets/iconNewMsgSmallGreen.png';
import iconNewMsgSmallGrey from '../../../../../assets/iconNewMsgSmallGrey.png';
import iconAddUserBlue from '../../../../../assets/iconAddUserBlue.png';
import iconAddedUserGrey from '../../../../../assets/iconAddedUserGrey.png';
import iconRemoveUserRed from '../../../../../assets/iconRemoveUserRed.png';
import iconRemoveUserGrey from '../../../../../assets/iconRemoveUserGrey.png';
import styles from './Contact.styles';

const useStyles = createUseStyles(styles);

function Contact({ contact, hasBeenAdded, correspondingChat, emit }) {
  const chatsDispatch = useContext(ChatsDispatchContext);
  const openedChatDispatch = useContext(OpenedChatDispatchContext);
  const contactsDispatch = useContext(ContactsDispatchContext);
  const [ confirmationIsOpen, setConfirmationIsOpen ] = useState(false);

  const { _id, username, blocked, blockedByContact } = contact;

  const handleStartChat = () => {
    if (blocked || blockedByContact)
      return;

    if (correspondingChat)
      setOpenedChat(correspondingChat, openedChatDispatch);
    else
      emit(START_CHAT, _id);

    handleAdd();
  };

  const handleAdd = () => {
    if (!hasBeenAdded) {
      emit(STORE_CONTACT, _id);
      addContact(contact, contactsDispatch);
    }
  };

  const handleRemove = () => {
    closeConfirmation();

    if (hasBeenAdded) {
      emit(DELETE_CONTACT, _id);
      removeContact(_id, contactsDispatch);
      
      if (correspondingChat)
        removeChat(correspondingChat._id, chatsDispatch);
    }
  };

  const toggleBlocked = () => {
    if (blocked)
      emit(UNBLOCK_CONTACT, _id);
    else
      emit(BLOCK_CONTACT, _id);

    updateContact(_id, { blocked: !blocked }, contactsDispatch);
  };

  const openConfirmation = () => {
    if (hasBeenAdded)
      setConfirmationIsOpen(true);
  };
  const closeConfirmation = () => setConfirmationIsOpen(false);

  const classes = useStyles({ hasBeenAdded, blocked, blockedByContact });

  return (
    <div className={classes.Contact}>
      {confirmationIsOpen && (
        <Confirmation
          title="Are you sure?"
          message={`Please confirm that you wish to remove ${username} from your contact list.`}
          confirm={handleRemove}
          cancel={closeConfirmation}
        />
      )}
      <div className={classes.userInfo}>
        <span className={classes.avatar} />
        <span>{username}</span>
      </div>
      <div className={classes.options}>
        <div className={`${classes.option} ${classes.optionStartChat}`}>
          <img
            className={classes.optionIcon}
            src={blocked || blockedByContact ? iconNewMsgSmallGrey : iconNewMsgSmallGreen}
            alt="start chat"
            onClick={handleStartChat}  
          />
          <div className={classes.optionDescription}>chat</div>
        </div>
        <div className={`${classes.option} ${classes.optionAdd}`}>
          <img
            className={classes.optionIcon}
            src={hasBeenAdded ? iconAddedUserGrey : iconAddUserBlue}
            alt="add contact"
            onClick={handleAdd} 
          />
          <div className={classes.optionDescription}>add</div>
        </div>
        <div className={`${classes.option} ${classes.optionRemove}`}>
          <img
            className={classes.optionIcon}
            src={hasBeenAdded ? iconRemoveUserRed : iconRemoveUserGrey}
            alt="remove contact"
            onClick={openConfirmation}  
          />
          <div className={classes.optionDescription}>remove</div>
        </div>
        <div className={`${classes.option} ${classes.optionBlock}`}>
          <ToggleBtn
            isOn={blocked}
            toggle={toggleBlocked}
            width={'40px'}
            height={'27px'}
          />
          <div className={classes.optionDescription}>block</div>
        </div>
      </div>
    </div>
  );
}

export default Contact;
