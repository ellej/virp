import commonStyles from '../../../../utils/commonStyles';

const styles = {
  ContactsList: {
    width: '100%',
    height: '100%',
    padding: '20px 50px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    overflowY: 'auto',
    scrollbarWidth: 'thin' // firefox
  },
  infoMsg: {
    marginBottom: '20px',
    fontSize: '12px',
    color: commonStyles.noisyWhite
  }
};

export default styles;
