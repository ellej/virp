import { useEffect, useRef } from 'react';
import { createUseStyles } from 'react-jss';
import { AnimatePresence } from 'framer-motion';
import ChatMessage from './ChatMessage/ChatMessage';
import styles from './ChatMessages.styles';

const useStyles = createUseStyles(styles);

function ChatMessages({ username, messages, chatPartner, decryptMsgByDefault, chatPartnerAccountDeleted }) {
  const msgsEndRef = useRef(null);

  const scrollToBottom = () => (
    msgsEndRef.current.scrollIntoView({
      //behavior: 'smooth',     // add this if i add a spinner when opening chat
      block: 'end',
      inline: 'nearest'
    })
  );
  useEffect(scrollToBottom, [messages]);

  const classes = useStyles();

  return (
    <div className={classes.ChatMessages}>
      <AnimatePresence>
        {messages.map(((message, idx) => (
          <ChatMessage
            key={idx}
            username={username}
            message={message}
            chatPartner={chatPartner}
            decryptMsgByDefault={decryptMsgByDefault}
            chatPartnerAccountDeleted={chatPartnerAccountDeleted}
          />
        )))}
        <div ref={msgsEndRef} />
      </AnimatePresence>
    </div>
  );
}

export default ChatMessages;
