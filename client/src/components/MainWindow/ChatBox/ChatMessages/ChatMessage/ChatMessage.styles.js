import commonStyles from '../../../../../utils/commonStyles';

const avatarDiameter = '50px';
const avatarMarginSide = '10px';

const styles = {
  ChatMessage: {
    maxWidth: '70%',
    maxHeight: '300px',
    alignSelf: ({ placeLeft }) => placeLeft ? 'flex-start' : 'flex-end',
    display: 'flex'
  },
  avatar: {
    width: avatarDiameter,
    height: avatarDiameter,
    marginLeft: ({ placeLeft }) => placeLeft ? 0 : avatarMarginSide,
    marginRight: ({ placeLeft }) => placeLeft ? avatarMarginSide : 0,
    order: ({ placeLeft }) => placeLeft ? 1 : 2,
    alignSelf: 'flex-start',
    fontSize: '20px',
    color: 'rgb(108, 108, 108)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '50%',
    border: '1px solid rgb(108, 108, 108)',
    boxShadow: commonStyles.boxShadowDownOutDarkLarge
  },
  msgBox: {
    order: ({ placeLeft }) => placeLeft ? 2 : 1,
    alignSelf: 'flex-end',
    height: '80%',
    maxWidth: () => `calc(100% - ${avatarDiameter} - ${avatarMarginSide})`,
    padding: '25px 30px 20px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    borderRadius: '30px',
    borderTopRightRadius: ({ placeLeft }) => placeLeft ? '30px' : 0,
    borderTopLeftRadius: ({ placeLeft }) => placeLeft ? 0 : '30px',
    border: '1px solid rgb(108, 108, 108)',
    boxShadow: commonStyles.boxShadowDownOutDarkLarge,
    overflowX: 'auto',
    overflowY: 'hidden',
    scrollbarWidth: 'thin',
    cursor: ({ isDecrypted }) => isDecrypted ? 'pointer' : 'default',
    '& input': {
      width: '85%',
      padding: '5px',
      fontSize: '12px',
      fontWeight: '400',
      textAlign: 'center',
      letterSpacing: '1px',
      backgroundColor: 'transparent',
      border: `1px solid ${commonStyles.noisyWhite}`,
      transition: 'all 200ms ease-in',
      '&:focus': {
        width: '100%',
        outline: 'none'
      },
      '&:required': {
        boxShadow: 'none'
      }
    }
  },
  msgBoxText: {
    fontSize: '12px',
    fontWeight: '400',
    letterSpacing: '1px',
    transition: 'all 200ms ease-in'
  },
  msgBoxMetaData: {
    marginTop: '15px',
    alignSelf: 'flex-end',
    fontSize: '10px',
    '& time, p': {
      color: commonStyles.quietGray,
      letterSpacing: '1px'
    }
  }
};

export default styles;
