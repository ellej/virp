import { useState, useContext } from 'react';
import Moment from 'react-moment';
import { createUseStyles } from 'react-jss';
import { motion } from 'framer-motion';
import { CryptoContext, CryptoSetContext } from '../../../../../contexts/crypto';
//import { OpenedChatDispatchContext } from '../../../../../contexts/chats';
import useCryptography from '../../../../../hooks/useCryptography';
import useInput from '../../../../../hooks/useInput';
import commonStyles from '../../../../../utils/commonStyles';
//import { removeMsg } from '../../../../../actions/chats';
import styles from './ChatMessage.styles';

const useStyles = createUseStyles(styles);

// note: later, when there is an incoming message, previous messages will be
// displayed as encrypted again, so use memo to only rerender when necessary.
// Also, when opening and closing chats, they will probably be encrypted again,
// so store the messages of chats that have been opened in memory

function ChatMessage({ username, message, chatPartner, decryptMsgByDefault, chatPartnerAccountDeleted }) {
  const { privKey, salt, privKeyIsDecrypted } = useContext(CryptoContext);
  const setCryptoData = useContext(CryptoSetContext);
  //const openedChatDispatch = useContext(OpenedChatDispatchContext);
  const [ msgIsDecrypted, setMsgIsDecrypted ] = useState(false);
  const [ needPassword, setNeedPassword ] = useState(false);
  //const [ fromNowDate, setFromNowDate ] = useState(message.createdAt);
  const [ password, handlePasswordChange, resetPassword ] = useInput('');
  const { privDecrypt, generateSymKey, symDecrypt } = useCryptography('');

  const currentUserIsSender = chatPartner._id === message.receiver;

  const getAvatar = () => {
    if (currentUserIsSender)
      return username.charAt(0).toUpperCase();

    return chatPartnerAccountDeleted
      ? <>&#10005;</>
      : chatPartner.username.charAt(0).toUpperCase();
  };

  const getDecryptedText = () => currentUserIsSender
    ? privDecrypt(message.msgEncryptedForSender, privKey)
    : privDecrypt(message.msgEncryptedForReceiver, privKey);

  const unlockMsg = () => {
    if (msgIsDecrypted)
      return;

    if (privKeyIsDecrypted)
      setMsgIsDecrypted(true);
    else
      setNeedPassword(true);
  };

  const getContent = () => {
    // if the user reloads the browser window, the private key will be
    // encrypted again. The decryption process uses the password,
    // thus the user has to provide the password if it is needed
    if (needPassword) {
      return (
        <form onSubmit={handlePasswordSubmit}>
          <input
            type="password"
            autoComplete="off"
            id="password"
            placeholder="Reconfirm password"
            value={password}
            onChange={handlePasswordChange}
            onKeyDown={listenForEnterKey}
            required
          />
        </form>
      );
    }

    if (msgIsDecrypted)
      return getDecryptedText();

    if (privKeyIsDecrypted)
      if (decryptMsgByDefault || currentUserIsSender)
        return getDecryptedText();
      
    return 'Click to unlock';
  };

  const listenForEnterKey = (e) => {
    const ENTER_KEY = 13;
    if (e.keyCode === ENTER_KEY)
      handlePasswordSubmit(e);
  };

  const handlePasswordSubmit = (e) => {
    e.preventDefault();

    if (!password)
      return;

    const res = decryptPrivKey();
    if (res.error)
      return;
    
    setNeedPassword(false);
    setMsgIsDecrypted(true);
    resetPassword();
  };

  const decryptPrivKey = () => {
    const symKey = generateSymKey(password, salt);
    const decryptedPrivKey = symDecrypt(privKey, symKey);
    const { error } = decryptedPrivKey;
    
    if (error)
      return { error };

    setCryptoData({
      privKey: decryptedPrivKey,
      salt,
      privKeyIsDecrypted: true
    });

    return true;
  };

  const handleExpiration = (expiresIn) => {
    /*const hasExpired = expiresIn.match(/in a minute/gi);
    //const hasExpired = expiresIn.match(/ago/gi);
    console.log(expiresIn);
    if (hasExpired) {
      console.log('removing msg');
      removeMsg(message._id, openedChatDispatch);
    }
    //else
      setFromNowDate(Date.now());*/
  };

  const getCreatedAtFormat = () => {
    // if less than 12 h ago
    //    show time 'ago'
    // if more than 12 h ago
    //    show 'Today at hh:mm'
  };

  const calendarStrings = {
    lastDay : '[Yesterday at] LT',
    sameDay : '[Today at] LT',
    nextDay : '[Tomorrow at] LT',
    lastWeek : '[Last] dddd [at] LT',
    nextWeek : 'dddd [at] LT',
    sameElse : 'MMM DD[,] YYYY [at] LT'
  };

  const placeLeft = !currentUserIsSender;
  const classes = useStyles({ placeLeft, msgIsDecrypted });

  return (
    <motion.div
      initial={commonStyles.pageTransitionVar.initial}
      animate={commonStyles.pageTransitionVar.animate}
      exit={commonStyles.pageTransitionVar.exit}
      variants={commonStyles.pageTransition.list}
      className={classes.ChatMessage}
    >
      <div className={classes.avatar}>
        {getAvatar()}
      </div>
      <div
        className={classes.msgBox}
        onClick={unlockMsg}
      >
        <div className={classes.msgBoxText}>
          {getContent()}
        </div>
        <div className={classes.msgBoxMetaData}>
          {/*<p>
            <Moment fromNow>{message.createdAt}</Moment>
          </p>
          <p>
            Expires&nbsp;
            <Moment
              to={message.expireAt}
              onChange={handleExpiration}
            >
              {fromNowDate}
            </Moment>
          </p>*/}
          <p>
            <Moment calendar={calendarStrings}>{message.createdAt}</Moment>
          </p>
        </div>
      </div>
    </motion.div>
  );
}

export default ChatMessage;
