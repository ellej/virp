import { useContext } from 'react';
import { createUseStyles } from 'react-jss';
import useInput from '../../../../../hooks/useInput';
import { SocketEmitContext } from '../../../../../contexts/socket';
import { ChatsDispatchContext } from '../../../../../contexts/chats';
import { updateChat } from '../../../../../actions/chats';
import { SET_MESSAGE_DESTRUCTION } from '../../../../../utils/socket';
import { expirations } from '../../../../../utils/msgExpirations';
import styles from './ChatSettingsBox.styles';

const useStyles = createUseStyles(styles);

function ChatSettingsBox({ chatId, chatMembers, userId, changeDestructionOpt, prevDestructionOpt, close }) {
  const emit = useContext(SocketEmitContext);
  const chatsDispatch = useContext(ChatsDispatchContext);
  const [ destructionOpt, handleDestructionChange, resetDestructionOpt ] = useInput('');

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!destructionOpt)
      return;

    if (prevDestructionOpt !== destructionOpt) {
      updateChat(                                                 // user for updating the context
        chatId,
        { members: getUpdatedMembers(destructionOpt) },
        chatsDispatch
      );
      changeDestructionOpt(destructionOpt);                       // used for updating the opened chat box
      emit(SET_MESSAGE_DESTRUCTION, { chatId, destructionOpt });  // used for database manipulation
    }

    resetDestructionOpt();
    close();
  };

  const getUpdatedMembers = (newDestructionOpt) => chatMembers.map(member =>
    member.user.toString() === userId
      ? { ...member, msgDestructionOpt: newDestructionOpt }
      : member
  );

  const classes = useStyles();

  return (
    <div className={classes.ChatSettingsBox}>
      <form onSubmit={handleSubmit}>
        {Object.values(expirations).map(opt => (
          <label
            key={opt}
            htmlFor={opt}
          > {opt}
            <input
              type="radio"
              id={opt}
              name="self_destruction"
              value={opt}
              checked={destructionOpt === opt}
              onChange={handleDestructionChange}
            />
            <span></span>
          </label>
        ))}
        <button type="submit">
          Set self-destruction
        </button>
        {/* add button to cancel (use the 'close' prop) */}
      </form>
    </div>
  );
}

export default ChatSettingsBox;
