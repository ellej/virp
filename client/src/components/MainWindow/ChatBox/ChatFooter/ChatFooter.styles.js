import commonStyles from '../../../../utils/commonStyles';

const styles = {
  ChatFooter: {
    width: '100%',
    height: '120px',
    padding: '20px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomRightRadius: commonStyles.mainWindowContentBox.borderRadius,
    borderBottomLeftRadius: commonStyles.mainWindowContentBox.borderRadius,
    boxShadow: commonStyles.boxShadowUpDarkMedium,
    zIndex: 1
  },
  content: {
    width: '100%',
    height: '100%',
    margin: '0 30px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    border: `1px solid ${commonStyles.quietWhite}`,
    borderBottomLeftRadius: '10px',
    borderBottomRightRadius: '10px'
  },
  options: {
    width: `calc(100% - ${commonStyles.typingBoxWidth})`,
    height: '100%',
    marginLeft: '20px',
    display: 'flex',
    justifyContent: 'space-evenly',
    alignItems: 'center'
  },
  option: {
    width: '40px',
    height: '40px',
    padding: '5px',
    borderRadius: '50%',
    opacity: 0.5,
    cursor: 'pointer',
    transition: 'all 150ms ease-in-out',
    '&:hover': {
      opacity: 1
    }
  }
};

export default styles;
