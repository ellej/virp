import { useContext, memo } from 'react';
import { createUseStyles } from 'react-jss';
import { AnimatePresence } from 'framer-motion';
import { UserContext } from '../../contexts/user';
import { ToastsContext } from '../../contexts/toasts';
import Toast from './Toast/Toast';
import { TOAST_NEW_MESSAGE } from '../../utils/toastTypes';
import styles from './Toasts.styles';

const useStyles = createUseStyles(styles);

function Toasts({ position }) {
  const toasts = useContext(ToastsContext);
  const user = useContext(UserContext);

  const getRelevantToasts = () => toasts.filter(t => (
    t.type !== TOAST_NEW_MESSAGE || (t.type === TOAST_NEW_MESSAGE && user.newMsgNotificationOn)
  ));

  const classes = useStyles(position);

  return (
    <div className={classes.Toasts}>
      <AnimatePresence>
        {getRelevantToasts().map(t => (
          <Toast
            key={t.id}
            id={t.id}
            type={t.type}
            msg={t.msg}
          />
        ))}
      </AnimatePresence>
    </div>
  );
}

export default memo(Toasts);
