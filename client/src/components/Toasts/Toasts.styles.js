import { TOAST_POS_RIGHT, TOAST_POS_BOTTOM } from '../../utils/toastPositions';

const getPositionAndLayout = (position) => {
  switch (position) {
    case TOAST_POS_RIGHT:
      return {
        width: '250px',
        position: 'absolute',
        display: 'flex',
        top: '110px',
        right: 0,
        flexDirection: 'column',
        alignItems: 'flex-end'
      };
    case TOAST_POS_BOTTOM:
      return {
        width: '250px',
        position: 'absolute',
        display: 'flex',
        bottom: '15px',
        left: '50%',
        flexDirection: 'row',
        justifyContent: 'center'
      }
    default:
      return {
        width: '250px',
        position: 'absolute',
        display: 'flex',
        top: '110px',
        right: 0,
        flexDirection: 'column',
        alignItems: 'flex-end'
      };
  }
};

const styles = {
  Toasts: (position) => getPositionAndLayout(position)
};

export default styles;
