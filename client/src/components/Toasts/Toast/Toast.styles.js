import commonStyles from '../../../utils/commonStyles';
import {
  TOAST_INFO, TOAST_SUCCESS, TOAST_NEW_MESSAGE, TOAST_WARNING, TOAST_ERROR
} from '../../../utils/toastTypes';

const getBgColor = (type) => {
  switch (type) {
    case TOAST_INFO:
      return commonStyles.blue;
    case TOAST_SUCCESS:
      return commonStyles.green;
    case TOAST_NEW_MESSAGE:
      return commonStyles.green;
    case TOAST_WARNING:
      return commonStyles.orange;
    case TOAST_ERROR:
      return commonStyles.red;
    default:
      return commonStyles.blue;
  }
};

const styles = {
  Toast: {
    width: '100%',
    minHeight: '50px',
    margin: '10px',
    padding: '15px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: '5px',
    boxShadow: commonStyles.boxShadowOutDarkMedium2,
    backgroundColor: (type) => getBgColor(type)
  },
  icon: {
    fontSize: '20px'
  },
  msg: {
    margin: '0 10px',
    fontSize: '12px',
    textAlign: 'center'
  },
  closeBtn: {
    fontSize: '14px',
    cursor: 'pointer'
  }
};

export default styles;
