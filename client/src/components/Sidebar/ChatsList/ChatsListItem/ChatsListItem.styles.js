import commonStyles from '../../../../utils/commonStyles';

const getUnreadMsgsBgColor = (isOpen, hasUnreadMsgs) => {
  if (isOpen)
    return commonStyles.quietBlack;

  return hasUnreadMsgs ? commonStyles.red : commonStyles.quietBlack;
};

const styles = {
  ChatsListItem: {
    width: '100%',
    height: '70px',
    minHeight: '70px',
    fontSize: '12px',
    padding: '0 20px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRight: ({ isOpen }) => isOpen ? `2px solid ${commonStyles.brandGoldMain}` : 'none',
    backgroundColor: ({ isOpen }) => isOpen ? commonStyles.quietBlack : 'transparent',
    cursor: 'pointer',
    transition: 'all 150ms ease-in-out',
    '&:hover': {
      backgroundColor: commonStyles.quietBlack
    }
  },
  avatar: {
    width: '30px',
    height: '20px',
    fontWeight: '600',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: ({ isOpen }) => isOpen ? commonStyles.brandGoldMain : commonStyles.quietGray,
    borderRadius: '30px',
    borderTopRightRadius: '0',
    borderTopLeftRadius: '30px',
    boxShadow: commonStyles.boxShadowOutGreySmall,
    transition: 'all 150ms ease-in-out',
    '& span': {
      color: commonStyles.brandDarkSecond
    }
  },
  infoBox: {
    display: 'flex',
    alignItems: 'center'
  },
  infoContent: {
    marginLeft: '20px',
    marginRight: '5px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  username: {
    marginBottom: '3px',
    color: ({ isOpen, chatPartnerAccountDeleted }) => {
      if (isOpen)
        return commonStyles.brandGoldMain;

      return chatPartnerAccountDeleted ? commonStyles.quietGray : 'white'
    }
  },
  time: {
    fontSize: '10px',
    color: commonStyles.quietGray
  },
  unreadMsgs: {
    width: '30px',
    height: '30px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: ({ hasUnreadMsgs }) => hasUnreadMsgs ? commonStyles.brandGoldMain : commonStyles.green,
    borderRadius: '5px',
    backgroundColor: ({ isOpen, hasUnreadMsgs }) => getUnreadMsgsBgColor(isOpen, hasUnreadMsgs),
    boxShadow: commonStyles.boxShadowOutDarkSmall
  }
};

export default styles;
