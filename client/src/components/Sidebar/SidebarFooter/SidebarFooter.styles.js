import commonStyles from '../../../utils/commonStyles';
import { OPENED_BOX_CONTACTS, OPENED_BOX_SETTINGS, OPENED_BOX_ABOUT } from '../../../utils/openedBox';

const footerOptHeight ='45px';

const styles = {
  SidebarFooter: {
    height: commonStyles.sidebarFooterHeight,
    borderTop: `1px solid ${commonStyles.quietWhite}`,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center'
  },
  footerTop: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  footerOpt: {
    width: footerOptHeight,
    padding: '8px',
    borderRadius: '5px',
    cursor: 'pointer',
    transition: 'all 150ms ease-in-out',
    '&:hover': {
      border: `1px solid ${commonStyles.brandGoldMain}`
    }
  },
  footerOptContacts: {
    border: openedBox => openedBox === OPENED_BOX_CONTACTS
      ? `1px solid ${commonStyles.brandGoldMain}`
      : `1px solid ${commonStyles.quietWhite}`,
  },
  footerOptSettings: {
    border: openedBox => openedBox === OPENED_BOX_SETTINGS
      ? `1px solid ${commonStyles.brandGoldMain}`
      : `1px solid ${commonStyles.quietWhite}`,
  },
  footerOptAbout: {
    border: openedBox => openedBox === OPENED_BOX_ABOUT
      ? `1px solid ${commonStyles.brandGoldMain}`
      : `1px solid ${commonStyles.quietWhite}`,
  },
  footerOptLogout: {
    width: '83%',
    height: footerOptHeight,
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    border: `1px solid  ${commonStyles.quietWhite}`,
    borderRadius: '5px',
    transition: 'all 150ms ease-in-out',
    '& span': {
      color: 'rgba(255, 255, 255, 0.2)',
      fontWeight: '600',
      transition: 'all 150ms ease-in-out'
    },
    '&:hover': {
      borderRadius: '20px',
      '& span': {
        color: commonStyles.brandGoldMain
      }
    }
  },
  footerOptLogoutIcon: {
    width: '23px',
    height: '23px',
    position: 'absolute',
    top: '50%',
    left: '20px',
    transform: 'translateY(-50%)',
    background: commonStyles.iconExitGrey,
    backgroundSize: '100%',
    opacity: 0.5
  }
};

export default styles;
