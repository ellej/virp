import Loader from 'react-loader-spinner';

function Loading({ type, color, height, width, timeout }) {
  return (
    <Loader
      type={type}
      color={color}
      height={height}
      width={width}
      timeout={timeout}
      ariaLabel='Loading'
    />
  );
}

export default Loading;
