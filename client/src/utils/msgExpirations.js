export const expirations = {
  ONE_MIN: '1m',
  FIVE_MIN: '5m',
  THIRTY_MIN: '30m',
  ONE_HOUR: '1h',
  SIX_HOURS: '6h',
  TWELVE_HOURS: '12h',
  ONE_DAY: '1d',
  SEVEN_DAYS: '7d',
  DEFAULT: '30d'
};
