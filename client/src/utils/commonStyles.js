import mediaQuerySizes from './mediaQuerySizes';
import iconLock from '../assets/lockIcon.png';
import iconLoginProfileWhite from '../assets/iconLoginProfileWhite.png';
import iconLoginProfileGold from '../assets/iconLoginProfileGold.png';
import iconProtectedWhite from '../assets/iconProtectedWhite.png';
import iconExitGrey from '../assets/iconExitGrey.png';
import iconNewMsgGreen from '../assets/iconNewMsgGreen.png';
import iconSearchGrey from '../assets/iconSearchGrey.png';

const brandGoldMain = '#e6a872';  // rgba(230, 168, 114, 1)
const brandGoldMainLowOpacity = 'rgba(230, 168, 114, 0.4)';
const brandDarkMain = '#202535';  // rgba(32, 37, 53, 1)
const brandDarkSecond = '#2b3045';

const headerHeight = '120px';

const commonStyles = {
  brandDarkMain: brandDarkMain,
  brandGoldMainLowOpacity: brandGoldMainLowOpacity,
  brandGoldMain: brandGoldMain,
  brandDarkSecond: brandDarkSecond,

  quietWhite: 'rgba(255, 255, 255, 0.1)',
  noisyWhite: 'rgba(255, 255, 255, 0.4)',
  quietGray: 'rgba(255, 255, 255, 0.3)',
  quietBlack: 'rgba(0, 0, 0, 0.2)',
  noisyBlack: 'rgba(0, 0, 0, 0.5)',
  green: 'rgba(24, 124, 29, 1)',
  red: 'rgba(123, 25, 25, 1)',
  orange: 'rgba(225, 137, 86, 1)',
  blue: 'rgba(115, 177, 231, 1)',

  mainTextFontFamily: 'Montserrat, sans-serif',

  headerHeight: headerHeight,
  sidebarWidth: '380px',
  sidebarMinWidth: '320px',
  sidebarHeaderHeight: '160px',
  sidebarFooterHeight: '17%',
  typingBoxWidth: '85%',
  settingsSidebarWidth: '30%',

  mainWindowContentBox: {
    width: '70%',
    height: '80%',
    paddingBottom: '40px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    background: 'linear-gradient(180deg, rgba(43,48,69,1) 0%, rgba(32,37,53,1) 100%)',
    boxShadow: '0px 0px 27px -6px rgba(23,23,23,1)',
    borderRadius: '15px'
  },
  mainWindowContentBoxHeader: {
    width: '100%',
    height: '74px',
    padding: '0 40px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    boxShadow: '0px 8px 15px -6px rgba(23,23,23,1)',    // boxShadowDownOutDarkMedium
    zIndex: 1
  },
  mainWindowContentBoxTitle: {
    borderLeft: `1px solid ${brandGoldMain}`,
    paddingLeft: '10px',
  },
  mainWindowContentBoxCloseBtn: {
    width: '30px',
    height: '30px',
    fontSize: '19px',
    paddingLeft: '2px',
    paddingBottom: '2px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    border: `1px solid ${brandGoldMain}`,
    borderRadius: '50%',
    cursor: 'pointer',
    transition: 'all 150ms ease-in-out',
    '&:hover': {
      boxShadow: '0px 0px 15px -6px rgba(230,168,114,1)'    // boxShadowOutGoldSmall
    }
  },

  settingsOptItem: {
    width: '100%',
    height: '80px',
    fontSize: '12px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '10px 0',
    borderBottom: '1px solid rgba(255, 255, 255, 0.1)',   // quietWhite
    transition: 'all 150ms ease-in-out',
    '& span': {
      transition: 'all 150ms ease-in-out'
    },
    '&:hover': {
      borderBottom: `1px solid ${brandGoldMain}`,
      paddingLeft: '10px',
      '& span, time': {
        color: brandGoldMain
      }
    }
  },

  deleteBtn: {
    width: '30px',
    height: '30px',
    display: 'inline-block',
    opacity: '0.7',
    cursor: 'pointer',
    transition: 'all 150ms ease-in-out',
    '&:hover': {
      opacity: 1
    }
  },
  backBtn: {
    position: 'fixed',
    bottom: '40px',
    '& a': {
      color: brandGoldMain
    },
    [mediaQuerySizes.maxWidth.sm()]: {
      fontSize: '14px'
    },
    [mediaQuerySizes.maxHeightLaptop.md()]: {
      bottom: '20px',
      fontSize: '14px'
    }
  },

  fullViewFlexColumn: {
    width: '100vw',
    height: '100vh',
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: brandDarkMain,
    overflow: 'hidden'
  },
  fullWidthContentCentered: {
    width: '100vw',
    height: `calc(100% - ${headerHeight})`,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },

  btnLarge: {
    width: '300px',
    height: '160px',
    fontSize: '50px',
    fontWeight: '900',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    outline: 'none',
    color: brandGoldMain,
    border: `4px solid ${brandGoldMain}`,
    background: 'transparent', // or background: 'none'
    transition: 'all 200ms ease-out',
    cursor: 'pointer',
    '&:hover': {
      width: '380px'
    },
    [mediaQuerySizes.maxWidth.md()]: {
      '&:hover': {
        width: '360px'
      }
    },
    [mediaQuerySizes.maxWidth.sm()]: {
      width: '280px',
      height: '150px',
      fontSize: '48px',
      '&:hover': {
        width: '340px'
      }
    },
    [mediaQuerySizes.maxWidth.xs()]: {
      width: '220px',
      height: '120px',
      fontSize: '38px',
      '&:hover': {
        width: '260px'
      }
    },
    [mediaQuerySizes.maxHeightLaptop.lg()]: {
      width: '280px',
      height: '150px',
      fontSize: '48px',
      '&:hover': {
        width: '340px'
      }
    }
  },

  searchBoxBorderRadius: '20px',
  searchBoxBgColor: 'rgba(255, 255, 255, 0.1)',
  searchIcon: {
    position: 'absolute',
    top: '50%',
    left: '20px',
    transform: 'translateY(-50%)',
    width: '20px',
    height: '20px',
    opacity: 0.7,
    background: `url(${iconSearchGrey}) 50% 50% no-repeat`,
    backgroundSize: '100%'
  },

  boxShadowOutDarkSmall: '0px 0px 5px 0px rgba(23,23,23,1)',
  boxShadowOutDarkMedium: '0px 0px 10px 0px rgba(23,23,23,1)',
  boxShadowOutDarkMedium2: '0px 0px 15px -6px rgba(23, 23, 23, 1)',
  boxShadowOutGreySmall: '0px 0px 5px 0px rgba(43,43,43,1)',
  boxShadowOutGreyMedium: '0px 0px 10px 0px rgba(43,43,43,1)',
  boxShadowOutGoldSmall: '0px 0px 15px -6px rgba(230,168,114,1)',
  boxShadowOutGoldMedium: '0px 0px 25px -6px rgba(230,168,114,1)',
  boxShadowDownOutDarkMedium: '0px 8px 15px -6px rgba(23,23,23,1)',
  boxShadowDownOutDarkLarge: '0px 8px 28px -6px rgba(23,23,23,1)',
  boxShadowUpDarkMedium: '0px -8px 15px -6px rgba(23,23,23,1)',

  infoBox: {
    width: '60%',
    maxHeight: '75%',
    padding: '30px 60px',
    fontSize: '14px',
    textAlign: 'center',
    borderRadius: '15px',
    boxShadow: '0px 0px 27px -6px rgba(23,23,23,1)',
    background: 'linear-gradient(180deg, rgba(43,48,69,1) 0%, rgba(32,37,53,1) 100%)',
    overflowY: 'scroll',
    scrollbarWidth: 'thin', // firefox
    '& p': {
      margin: '30px 0',
      lineHeight: '2em'
    },
    [mediaQuerySizes.maxWidth.lg()]: {
      width: '70%'
    },
    [mediaQuerySizes.maxWidth.sm()]: {
      padding: '30px 40px'
    },
    [mediaQuerySizes.maxWidth.xs()]: {
      width: '80%'
    },
    [mediaQuerySizes.maxHeightLaptop.lg()]: {
      padding: '30px 40px'
    },
  },
  infoBoxTitle: {
    fontWeight: '700',
    marginBottom: '30px'
  },

  pageTransition: {
    upToDown: {
      initial: { opacity: 0, y: '-100%' },
      animate: { opacity: 1, y: 0 },
      exit: { opacity: 0, y: '-100%' },
    },
    downToUp: {
      initial: { opacity: 0, y: '100%' },
      animate: { opacity: 1, y: 0 },
      exit: { opacity: 0, y: '100%' },
    },
    fade: {
      initial: { opacity: 0 },
      animate: { opacity: 1 },
      exit: { opacity: 0 },
    },
    horizontal: {
      initial: { opacity: 0, x: '-100vw', scale: 0.8 },
      animate: { opacity: 1, x: 0, scale: 1 },
      exit: { opacity: 0, x: '-100vw', scale: 0.8 }
    },
    popInOut: {
      initial: { opacity: 0, scale: 0.2 },
      animate: { opacity: 1, scale: 1 },
      exit: { opacity: 0, scale: 0.2 }
    },
    list: {
      initial: { opacity: 0, x: '-100%' },
      animate: { opacity: 1, x: 0 },
      exit: { opacity: 0, x: '-100%' }
    }
  },
  customTransition: {
    tween: {
      type: 'tween',
      ease: 'anticipate',
      duration: 0.2
    }
  },
  pageTransitionVar: {
    initial: 'initial',
    animate: 'animate',
    exit: 'exit',
  },

  backgroundLock: `url(${iconLock}) 50% 50% no-repeat`,
  iconLoginProfileWhite: `url(${iconLoginProfileWhite}) 50% 50% no-repeat`,
  iconLoginProfileGold: `url(${iconLoginProfileGold}) 50% 50% no-repeat`,
  iconProtectedWhite: `url(${iconProtectedWhite}) 50% 50% no-repeat`,
  iconExitGrey: `url(${iconExitGrey}) 50% 50% no-repeat`,
  iconNewMsgGreen: `url(${iconNewMsgGreen}) 50% 50% no-repeat`
};

export default commonStyles;
