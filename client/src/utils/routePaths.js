export const ROUTE_HOME = '/';

export const ROUTE_SIGNUP = '/signup';

export const ROUTE_LOGIN = '/login';

export const ROUTE_DASHBOARD = '/dashboard';

export const ROUTE_ABOUT = '/about';
