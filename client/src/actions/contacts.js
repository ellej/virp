import {
  ADD_CONTACTS, ADD_CONTACT, UPDATE_CONTACT, ADD_SEARCH_RES, REMOVE_CONTACT
} from './actionTypes';

export const addContacts = (contacts, dispatch) => {
  dispatch({
    type: ADD_CONTACTS,
    contacts
  });
};

export const addContact = (contact, dispatch) => {
  dispatch({
    type: ADD_CONTACT,
    contact
  });
};

export const updateContact = (contactId, updates, dispatch) => {
  dispatch({
    type: UPDATE_CONTACT,
    contactId,
    updates
  });
};

export const addSearchRes = (res, dispatch) => {
  dispatch({
    type: ADD_SEARCH_RES,
    res
  });
};

export const removeContact = (id, dispatch) => {
  dispatch({
    type: REMOVE_CONTACT,
    id
  });
}
