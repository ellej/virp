import { UPDATE_USER } from './actionTypes';

export const updateUser = (updates, dispatch) => {
  dispatch({
    type: UPDATE_USER,
    updates
  });
};
