// user
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const UPDATE_USER = 'UPDATE_USER';

// chats
export const ADD_CHATS = 'ADD_CHATS';
export const ADD_CHAT = 'ADD_CHAT';
export const UPDATE_CHAT = 'UPDATE_CHAT';
export const REMOVE_CHAT = 'REMOVE_CHAT';

// opened chat
export const SET_OPENED_CHAT = 'SET_OPENED_CHAT';
export const ADD_CHAT_MSGS = 'ADD_CHAT_MSGS';
export const ADD_NEW_MSG = 'ADD_NEW_MSG';
export const REMOVE_MSG = 'REMOVE_MSG';

// contacts
export const ADD_CONTACTS = 'ADD_CONTACTS';
export const ADD_CONTACT = 'ADD_CONTACT';
export const UPDATE_CONTACT = 'UPDATE_CONTACT';
export const ADD_SEARCH_RES = 'ADD_SEARCH_RES';
export const REMOVE_CONTACT = 'REMOVE_CONTACT';

// unread messages
export const ADD_UNREAD_MSGS = 'ADD_UNREAD_MSGS';
export const ADD_UNREAD_MSG = 'ADD_UNREAD_MSG';
export const MARK_AS_READ = 'MARK_AS_READ';

// toasts
export const ADD_TOAST = 'ADD_TOAST';
export const REMOVE_TOAST = 'REMOVE_TOAST';
