import {
  ADD_CHATS, ADD_CHAT, UPDATE_CHAT, REMOVE_CHAT, SET_OPENED_CHAT,
  ADD_CHAT_MSGS, ADD_NEW_MSG, REMOVE_MSG
} from './actionTypes';

// for all active chats

export const addChats = (chats, dispatch) => {
  dispatch({
    type: ADD_CHATS,
    chats
  });
};

export const addChat = (chat, dispatch) => {
  dispatch({
    type: ADD_CHAT,
    chat
  });
};

export const updateChat = (id, updates, dispatch) => {
  dispatch({
    type: UPDATE_CHAT,
    id,
    updates
  });
};

export const removeChat = (id, dispatch) => {
  dispatch({
    type: REMOVE_CHAT,
    id
  });
};


// for the chat currently opened

export const setOpenedChat = (chat, dispatch) => {
  dispatch({
    type: SET_OPENED_CHAT,
    chat
  });
};

export const addChatMsgs = (msgs, dispatch) => {
  dispatch({
    type: ADD_CHAT_MSGS,
    msgs
  });
};

export const addNewMsg = (newMsg, chatId, dispatch) => {
  dispatch({
    type: ADD_NEW_MSG,
    newMsg,
    chatId
  });
};

export const removeMsg = (msgId, dispatch) => {
  dispatch({
    type: REMOVE_MSG,
    msgId
  });
};
