import axios from 'axios';

const http = async (method, url, data) => {
  try {
    const res = await axios[method.toLowerCase()](url, data);

    return res.data;
  }
  catch (err) {
    // err.response.data includes an error object by the server:
    // { error: { code, message } }
    return err.response.data;
  }
};

export default http;
