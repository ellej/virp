import http from './http';
import { API_URL, API_USERS_BY_ID_ENDPOINT, API_USERS_BY_NAME_ENDPOINT } from '../config/urls';

const getUser = (endpoint) => {
  const apiEndpoint = API_URL + endpoint;

  return http('GET', apiEndpoint);
};

export const getUserByUsername = (username) => {
  const endpoint = API_USERS_BY_NAME_ENDPOINT + '/' + username;

  return getUser(endpoint);
};

export const getChatPartner = (chat, me) => {
  const endpoint = API_USERS_BY_ID_ENDPOINT + '/' + getChatPartnerId(chat, me);

  return getUser(endpoint);
}

const getChatPartnerId = (chat, me) => (
  // there are only 2 members in each chat, thus the chat partner's
  // id is the one that does not match the current user's (me) id
  chat.members
    .filter(member => member.user.toString() !== me._id)[0]
    .user
);

const updateUser = (endpoint, updates) => {
  const apiEndpoint = API_URL + endpoint;

  return http('PUT', apiEndpoint, updates);
};

export const updateUserById = (id, updates) => {
  const endpoint = API_USERS_BY_ID_ENDPOINT + '/' + id;

  return updateUser(endpoint, updates);
};
