import axios from 'axios';
import http from './http';
import { setToken, removeToken } from '../utils/auth';
import { API_URL } from '../config/urls';

export const loginOrSignup = (endpoint, payload) => {
  const apiEndpoint = API_URL + endpoint;

  return http('POST', apiEndpoint, payload);
};

export const setAuth = (token) => {
  setToken(token);
  setTokenHeader(token);
};

export const removeAuth = () => {
  removeToken();
  setTokenHeader(false);
};

// pass a token to this function to set a token
// pass a falsy argument to delete the token
export const setTokenHeader = (token) => {
  if (token)
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  else
    delete axios.defaults.headers.common['Authorization'];
};
