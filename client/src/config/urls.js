// server
const SERVER_URL_PROD = 'https://virp.glitch.me';
const SERVER_URL_DEV = 'http://localhost:3001';

export const SERVER_URL = import.meta.env.PROD ? SERVER_URL_PROD : SERVER_URL_DEV;

// api
export const API_URL = SERVER_URL + '/api/v1';

// api auth
export const API_SIGNUP_ENDPOINT = '/auth/signup';
export const API_LOGIN_ENDPOINT = '/auth/login';

// api users
export const API_USERS_BY_NAME_ENDPOINT = '/users/username';
export const API_USERS_BY_ID_ENDPOINT = '/users/id';

// api wake up the server
export const API_WAKE_UP_ENDPOINT = '/wake-up';
